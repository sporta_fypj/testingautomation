﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using Excel = Microsoft.Office.Interop.Excel;

namespace UnitTestBot
{
    class NYPChatBotDev2Tabs
    {
        IWebDriver driver;
        Excel.Application botApp;
        Excel.Workbook botWorkbook;
        Excel._Worksheet botWorksheet;
        readonly string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\..\..\..\..\..";


        // init (change according to excelsheet)
        List<string> whitelistWorkSheets = new List<string> { "SIT" };

        int questionRowNumber = 1;

        int answerRowNumber = 2; // expected reply

        int statusRowNumber = 6;


        // development mode
        readonly bool devMode = true;

        public NYPChatBotDev2Tabs()
        {
            botApp = new Excel.Application();

            botWorkbook = botApp.Workbooks.Open(filePath + @"\03. SampleQnA\SITQnAForFYPJ");
        }

        [SetUp]
        public void Startbrowser()
        {
            driver = new InternetExplorerDriver()
            {
                Url = "https://asknypadmindev.azurewebsites.net/botmain"
            };

            driver.Manage().Window.Maximize();
        }

        [Test]
        public void NYPChatBotIE()
        {
            // open chatbot by clicking on img
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

            IWebElement NYPChatBotImg = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));

            NYPChatBotImg.Click();

            Thread.Sleep(500); // some mercy for animation, can delete

            // default msg count, do not change
            int tab1MsgCount = 2;

            int tab2MsgCount = 2;


            // for devMode = true
            int remarksRowNumber = 5;

            int actualReplyRowNumber = 7;

            int expectedReplyRowNumber = 8;


            // select General Enquiries tab as starting tab (can set default as "l1")
            string currTab = "l2";

            IWebElement textbox = SelectTab(currTab);
            IWebElement submitButton = GetSubmitButton(currTab);

            for (int whitelistIndex = 0; whitelistIndex < whitelistWorkSheets.Count; whitelistIndex++)
            {
                int currentColumnNumber = 2;

                bool tabSwitched = false;

                try
                {
                    botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item(whitelistWorkSheets[whitelistIndex]);

                    botWorksheet.Cells[statusRowNumber][1].Value2 = "Status";

                    if (devMode)
                    {
                        botWorksheet.Cells[actualReplyRowNumber][1].Value2 = "Actual reply (refined)";
                        botWorksheet.Cells[expectedReplyRowNumber][1].Value2 = "Expected reply (refined)";
                    }
                }
                catch (Exception ex)
                {
                    Console.Write(ex); // sheet name does not exist

                    continue; // next sheet
                }


                while (!string.IsNullOrWhiteSpace(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2) && !string.IsNullOrWhiteSpace(botWorksheet.Cells[answerRowNumber][currentColumnNumber].Value2))
                {

                    try
                    {


                        // Ask question from excel sheet
                        textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

                        //textbox.SendKeys(Keys.Enter);
                        submitButton.Click();
                        textbox.Click();

                        string actualReply = "";

                        string messageFound = "";

                        bool shouldDelay = false;


                        // Find question chat bubble and get next reply
                        if (currTab == "l1")
                        {
                            while (true)
                            {
                                try
                                {


                                    // driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(1);
                                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(2);
                                    messageFound = _removeNestedWhitespaces(_trimReply(driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText")));


                                    if (messageFound == _removeNestedWhitespaces(_trimReply(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2)))
                                    {
                                        try
                                        {
                                            tab1MsgCount += 1;

                                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(8);

                                            // actualReply = RefineReply(driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText"));
                                            actualReply = RefineReply(driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + 1 + "]/div/div")).GetAttribute("innerText"));
                                            break;
                                        }
                                        catch
                                        {
                                            //// Ask question again
                                            //textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

                                            ////textbox.SendKeys(Keys.Enter);
                                            //submitButton.Click();
                                            //textbox.Click();

                                            //shouldDelay = true;

                                            //continue; // search for this question


                                            //// NEXT
                                            //actualReply = "NO REPLY FOUND";

                                            //break;


                                            // ask question again at tab 2
                                            actualReply = "For non course-related queries (e.g. facilities), you may find out from General Enquiries.Useful | Not Useful | Comment";

                                            break;
                                        }
                                    }
                                    else
                                    {
                                        tab1MsgCount += 1;
                                    }
                                }
                                catch
                                {
                                    botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = _removeNestedWhitespaces(_trimReply(driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[16]/div/div")).GetAttribute("innerText")));
                                    break;
                                }
                            }

                            if (actualReply == "For non course-related queries (e.g. facilities), you may find out from General Enquiries.Useful | Not Useful | Comment")
                            {
                                if (tabSwitched)
                                {
                                    // dead question
                                    actualReply = "No response";
                                }
                                else
                                {
                                    currTab = "l2";

                                    textbox = SelectTab(currTab);
                                    submitButton = GetSubmitButton(currTab);

                                    tabSwitched = true;

                                    continue; // ask question again at tab 2

                                }
                            }
                        }
                        else if (currTab == "l2")
                        {
                            while (true)
                            {
                                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(8);
                                messageFound = driver.FindElement(By.XPath("//*[@id='bot']/div/div/div[2]/div/div/div[" + tab2MsgCount + "]/div/div/div")).GetAttribute("innerText");
                                messageFound = Message(messageFound);
                                botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = messageFound;

                                //    messageFound = _removeNestedWhitespaces(_trimReply(driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div")).GetAttribute("innerText")));
                                //  botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = messageFound;
                                
                                if (messageFound == RefineMsg(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2))
                                {
                                    try
                                    {
                                        tab2MsgCount += 1;

                                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(12);

                                        //actualReply = RefineReply(driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div/div")).GetAttribute("innerText"));

                                        actualReply = RefineReply(driver.FindElement(By.XPath("//*[@id='bot']/div/div/div[2]/div/div/div[" + tab2MsgCount + "]/div/div/div/div")).GetAttribute("innerText"));

                                        // check if it is the feedback message and get reply after feedback message (happens once in a blue moon where the question is asked before the feedback msg is received)
                                        if (actualReply == "FeedbackTo help me improve in answering your future question(s), please choose whether you \"👍\" or \"👎\" our answers.Like 👍Dislike 👎Comment")
                                        {
                                            tab2MsgCount += 1;

                                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(12);



                                            //actualReply = RefineReply(driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div/div")).GetAttribute("innerText"));

                                            actualReply = RefineReply(driver.FindElement(By.XPath("//*[@id='bot']/div/div/div[2]/div/div/div[" + tab2MsgCount + "]/div/div/div/div")).GetAttribute("innerText"));
                                        }

                                        break;
                                    }
                                    catch
                                    {
                                        // Ask question again
                                        textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

                                        //textbox.SendKeys(Keys.Enter);
                                        submitButton.Click();
                                        textbox.Click();

                                        shouldDelay = true;

                                        continue; // search for this question
                                    }
                                }
                                else
                                {
                                    tab2MsgCount += 1;
                                }
                            }

                            if (actualReply == "For the above question, you may find out from Full-time Course & Admission Advisory.")
                            {
                                if (tabSwitched)
                                {
                                    // dead question
                                    actualReply = "No response";
                                }
                                /*
                                else
                                {
                                    currTab = "l1";

                                    textbox = SelectTab(currTab);
                                    submitButton = GetSubmitButton(currTab);

                                    tabSwitched = true;

                                    continue; // ask question again at tab 1
                                }
                                */
                            }
                        }

                        string expectedReply = RefineReply(botWorksheet.Cells[answerRowNumber][currentColumnNumber].Value2);

                        if (devMode)
                        {
                            botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;

                            botWorksheet.Cells[expectedReplyRowNumber][currentColumnNumber].Value2 = expectedReply;
                        }


                        // Refine, compare and update status
                        if (actualReply == "No response")
                        {
                            botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";
                        }
                        else
                        {
                            string comparedResults = CompareReplies(actualReply, expectedReply, botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2);

                            if (comparedResults == "both replies are the same")
                            {
                                botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
                            }
                            else
                            {
                                botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";

                                if (devMode) // remarks
                                {
                                    botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = comparedResults;
                                }
                            }
                        }

                        tabSwitched = false; // reset var

                        if (shouldDelay) // question failed to send
                        {
                            Thread.Sleep(100); // for the reasked questions to spam answers
                        }

                        currentColumnNumber += 1; // next question
                    }
                    catch(Exception ex)
                    {
                        Console.Write(ex);
                        currentColumnNumber += 1;
                    }
                }
            }
        }

        [TearDown]
        public void CloseBrowser()
        {
            if (devMode)
            {
                // save to new excelsheet
                int count = 1;
                while (File.Exists(filePath + @"\TestResults\SampleQnATestResults_IE" + count + ".xlsx"))
                {
                    count += 1;
                }

                botWorkbook.SaveAs(filePath + @"\TestResults\SampleQnATestResults_IE" + count + ".xlsx");
            }
            else
            {
                // save in same excel
                botWorkbook.Save();
            }

            botWorkbook.Close();

            botApp.Quit();

            // release all COM object (Close Excel properly)
            if (botWorksheet != null) Marshal.ReleaseComObject(botWorksheet);
            if (botWorkbook != null) Marshal.ReleaseComObject(botWorkbook);
            if (botApp != null) Marshal.ReleaseComObject(botApp);

            Thread.Sleep(1000);

            driver.Quit(); // End session
        }


        private IWebElement SelectTab(string tabID)
        {
            IWebElement messageTextbox;

            driver.SwitchTo().DefaultContent();

            // select tab
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            IWebElement NYPChatBotTab = driver.FindElement(By.XPath(".//*[@id='l2']"));
           // IWebElement NYPChatBotTab = driver.FindElement(By.XPath(".//*[@id='" + tabID + "']"));
            NYPChatBotTab.Click();

            // select search textbox
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            if (tabID == "l1")
            {
                IWebElement tab1IFrame = driver.FindElement(By.XPath("//*[@id='polyBot']"));

                driver.SwitchTo().Frame(tab1IFrame);

                messageTextbox = driver.FindElement(By.XPath("//*[@id='textInput']"));
            }
            else //if (tabID == "l2")
            {
             //   messageTextbox = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[2]/div/input"));
                IWebElement tab2IFrame = driver.FindElement(By.XPath("//*[@id='nypBot']"));
                driver.SwitchTo().Frame(tab2IFrame);
                messageTextbox = driver.FindElement(By.XPath("//*[@id='bot']/div/div/div[3]/div/input"));
            }
            //else /* default */
            //{
            //    messageTextbox = driver.FindElement(By.XPath("//*[@id='textInput']"));
            //}

            messageTextbox.Click();

            return messageTextbox;
        }

        // init submit button (because IE with security and stuff)
        private IWebElement GetSubmitButton(string tabID)
        {

            if (tabID == "l1")
            {
                return driver.FindElement(By.XPath("//*[@id='arrowBtn']"));
            }
            else //if (tabID == "l2")
            {
                //return driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[2]/button[1]"));
                return driver.FindElement(By.XPath("//*[@id='bot']/div/div/div[3]/button"));
            }
            //else /* default */
            //{
            //    return driver.FindElement(By.XPath("//*[@id='arrowBtn']"));
            //}
        }

        private string RefineMsg(string msg)
        {
            msg = _trimReply(msg);

            msg = _removeNestedWhitespaces(msg);

            return msg.TrimEnd();

        }
        private string RefineReply(string reply)
        {
            reply = _trimReply(reply);

            reply = _removeTags(reply);

          //  reply = _removeNestedWhitespaces(reply);

            var newString = reply.Replace(".", ". ");
            newString = newString.Replace(";", "; ");
            newString = newString.Replace(": ", ":  ");
            reply = _removeNestedWhitespaces(newString);

            return reply;
        }

        private string Message(string msgFound)
        {
            msgFound = _removeNestedWhitespaces(msgFound);

            msgFound = _trimReply(msgFound);

            return msgFound.TrimEnd();
        }
        private string _trimReply(string reply)
        {
            Regex.Replace(reply, "\u00A0", " ");

            char[] delimitersChars = { '\n', '\r' };

            string[] replies = reply.Split(delimitersChars);

            string trimmedReplies = "";

            for (int i = 0; i < replies.Length; i++)
            {
                trimmedReplies += _ignoreList(replies[i]);
            }

            Regex.Replace(trimmedReplies, @"\t|\n|\r", " ");

            // not working! failing to remove smart (curly) quotations
            Regex.Replace(trimmedReplies, @"’", "\'");
            Regex.Replace(trimmedReplies, "\u2018", "\'");
            Regex.Replace(trimmedReplies, "\u2019", "\'");
            Regex.Replace(trimmedReplies, "\u201b", "\'");
            Regex.Replace(trimmedReplies, "\u2032", "\'");

            return trimmedReplies;
        }

        private string _ignoreList(string text)
        {
            string newText = text;

            if (!string.IsNullOrWhiteSpace(text))
            {
                char[] charArray = text.ToCharArray();

                if (char.IsDigit(charArray[0]))
                {
                    for (int i = 1; i < charArray.Length; i++)
                    {
                        if (charArray[i].Equals(')') || charArray[i].Equals('.')) // is a list
                        {
                            newText = text.Substring(i + 1).Trim();

                            break;
                        }
                        else if (!char.IsWhiteSpace(charArray[i])) // is not a list
                        {
                            break;
                        }
                    }
                }
                else if (charArray[0].Equals('-'))
                {
                    newText = text.Substring(1).Trim(); // is a list
                }
                else if (text.Contains("•"))
                {
                    for (int i = 0; i < charArray.Length; i++)
                    {
                        if (charArray[i].Equals('•')) // is a list
                        {
                            newText = text.Substring(i + 1).Trim();

                            break;
                        }
                        else if (!char.IsWhiteSpace(charArray[i])) // is not a list
                        {
                            break;
                        }
                    }
                }
            }

            return newText;
        }

        private string _removeTags(string text) // Can't use innerHTML due to constrain where questions and answers cannot contain '<' as it will break everything
        {
            bool isTag = false;

            string newText = "";

            char[] charArray = text.ToCharArray();

            for (int i = 0; i < charArray.Length; i++)
            {
                if (isTag)
                {
                    if (charArray[i].Equals('>'))
                    {
                        isTag = false;
                    }
                    //else  /* ignore */
                    //{
                    //    continue;
                    //}
                }
                else
                {
                    if (charArray[i].Equals('<'))
                    {
                        isTag = true;
                    }
                    else
                    {
                        newText += charArray[i];
                    }
                }
            }

            return newText;
        }

        private string _removeNestedWhitespaces(string str) // remove all double space (or more) in the middle of sentences
        {
            string[] strList = str.Split(' ');

            string newStr = "";

            foreach (string i in strList)
            {
                if (!string.IsNullOrWhiteSpace(i))
                {
                    newStr += i + " ";
                }
            }

            return newStr.Trim(); // .Trim() to remove space at the end
        }


        // called to check if replies (refined) are the same, also used to dictate which character(s) to ignore
        private string CompareReplies(string actualReply, string expectedReply, string currentRemarks)
        {
            string remarks = currentRemarks + "\n\n Sng Jing Qi:";

            char[] actual = actualReply.ToCharArray();

            char[] expected = expectedReply.ToCharArray();

            if (actual.Length != expected.Length)
            {
                remarks += "\n - the length of both result are different";
            }

            try
            {
                for (int i = 0; i < actual.Length; i++)
                {
                    if (actual[i] != expected[i])
                    {
                        if (expected[i] == '\'' || expected[i] == '\"') // ignore quotations
                        {
                            continue;
                        }
                        else if (expected[i] == ' ' && expected[i - 1] == ' ')
                        {
                            remarks += "\n - character at " + (i).ToString() + " contains a double space in the expected result";
                        }
                        else if (actual[i] == ' ' && actual[i - 1] == ' ')
                        {
                            remarks += "\n - character at " + (i).ToString() + " contains a double space in the actual result";
                        }
                        else
                        {
                            remarks += "\n - character at " + (i + 1).ToString() + " is different for both results";
                        }

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }

            if (remarks == currentRemarks + "\n\n Sng Jing Qi:")
            {
                remarks = "both replies are the same";
            }

            return remarks;
        }
    }
}