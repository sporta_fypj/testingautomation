﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Excel = Microsoft.Office.Interop.Excel;

/* Possible additions:
 * Spelling checker
 * 
 */

namespace UnitTestBot
{
    class BackUp5
    {
        IWebDriver driver;
        IWebElement msgContent;
        Excel.Application botApp;
        Excel.Workbook botWorkbook;
        Excel._Worksheet botWorksheet;
        Excel.Range botRange;
        readonly string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\..\..\..\..\..";
        //string currTab = "l1";

        public BackUp5()
        {
            botApp = new Excel.Application();
            //botWorkbook = botApp.Workbooks.Open(@"C:\Users\L33524\Documents\JingQi_baseline project\03. SampleQnA\SampleQnATest");
            //botWorkbook = botApp.Workbooks.Open(filePath + @"\03. SampleQnA\QnABankMutlipleSheets");
            botWorkbook = botApp.Workbooks.Open(filePath + @"\03. SampleQnA\QnABankForFYPJstudent");
        }

        [SetUp]
        public void Startbrowser()
        {
            driver = new ChromeDriver(filePath + @"\02. Software\chromedriver_win32");

            driver.Url = "https://asknypadmindev.azurewebsites.net/botwebchat";

            driver.Manage().Window.Maximize();
        }

        //[Test]
        public void NYPChatBot2Tabs()
        {
            //try
            //{
            //    //Click on the AskNYP Chatbot image
            //    IWebElement nypbotImage = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));
            //    nypbotImage.Click();

            //    Thread.Sleep(600); // give animation some mercy

            //    //// wait for the following element is be present
            //    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);


            //    //// select general enquiries tab
            //    //IWebElement generalEnquiriesTab = driver.FindElement(By.XPath(".//*[@id='l2']"));
            //    //generalEnquiriesTab.Click();

            //    //Thread.Sleep(500); // give animation some mercy

            //    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);


            //    IWebElement messageTextbox = SwitchTab();


            //    /* init excelsheet */
            //    List<string> whitelistWorkSheets = new List<string> { "SIT", "SEG", "SBM" };

            //    int questionRowNumber = 1;
            //    int answerRowNumber = 2; // expected reply
            //    int remarksRowNumber = 6;
            //    int statusRowNumber = 7;
            //    int actualReplyRowNumber = 8; // to record incorrect replies
            //    int expectedReplyRowNumber = 9;

            //    for (int whitelistIndex = 0; whitelistIndex < whitelistWorkSheets.Count; whitelistIndex++)
            //    {
            //        int currentColumnNumber = 2; // reset starting column /*45 sit*/

            //        try
            //        {
            //            botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item(whitelistWorkSheets[whitelistIndex]);
            //            botRange = botWorksheet.UsedRange;

            //            botWorksheet.Cells[statusRowNumber][1].Value2 = "Status";
            //            botWorksheet.Cells[actualReplyRowNumber][1].Value2 = "Actual reply (refined)";
            //            botWorksheet.Cells[expectedReplyRowNumber][1].Value2 = "Expected reply (refined)";
            //        }
            //        catch (Exception ex)
            //        {
            //            Console.Write(ex); // sheet name does not exist

            //            continue; // next sheet
            //        }

            //        int msgCount = 0;

            //        // +1 after minus because the starting column is deleted from the total columns. could -1 then minus, but it may be a little more confusing
            //        int totalQuestions = botWorksheet.Columns.Count - currentColumnNumber + 1;

            //        for (int index = 0; index < totalQuestions; index++)
            //        {
            //            /* TODO:
            //             * - Don't use count to find messages
            //             * -- get last msg? (if chat bot have not reply, check if the msg bubble is empty or question asked. if msg is feedback... black list?)
            //             * -- save a count and get msg count to compare and do some math? (compare previous count and current count? )
            //             */
            //            try
            //            {
            //                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

            //                // Enter question from excelsheet into textbox
            //                messageTextbox.SendKeys(Convert.ToString(botRange.Cells[questionRowNumber][currentColumnNumber].Value2));


            //                Thread.Sleep(300); // slight delay incase feedback message shows

            //                // track current message count
            //                msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*[div]")).Count;


            //                // Send Question
            //                messageTextbox.SendKeys(Keys.Enter);

            //                /* TODO:
            //                 * - Click retry button?
            //                 * - Resend question if reply not found?
            //                 */
            //                try
            //                {
            //                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            //                    // Goes to div.wc-message-content > div > div  // msgCount + 1 = first reply from bot, other replies will be ignored (e.g. feedback / more question with options)
            //                    msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(msgCount + 2) + "]/div[1]/div/div/div"));
            //                }
            //                catch
            //                {
            //                    try
            //                    {
            //                        botWorksheet.Cells[expectedReplyRowNumber + 1][currentColumnNumber].Value2 = msgCount;
            //                        msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*")).Count;
            //                        //botWorksheet.Cells[expectedReplyRowNumber + 1 + 1][currentColumnNumber].Value2 = msgCount;

            //                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            //                        msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(msgCount) + "]/div[1]/div/div/div"));
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = ex.ToString();
            //                        continue;
            //                    }
            //                }
            //                // compare the answers without tab, next line and return carriage
            //                string actualReply = refineReply(msgContent.GetAttribute("innerText"));
            //                string expectedReply = refineReply(botRange.Cells[answerRowNumber][currentColumnNumber].Value2);

            //                //string actualReply = Regex.Replace(Regex.Replace(msgContent.GetAttribute("innerText"), "\u00A0", " "), @"\t|\n|\r", "");
            //                //string expectedReply = Regex.Replace(Regex.Replace(botRange.Cells[answerRowNumber][currentColumnNumber].Value2, "\u00A0", " "), @"\t|\n|\r", "");

            //                //botWorksheet.Cells[expectedReplyRowNumber + 1][currentColumnNumber].Value2 = msgCount + 2;
            //                //msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*")).Count;
            //                //botWorksheet.Cells[expectedReplyRowNumber + 1 + 1][currentColumnNumber].Value2 = msgCount;

            //                /* IN PROGRESS: 
            //                 * - Check if result is 'For the above question, you may find out from Full-time Course & Admission Advisory.'
            //                 * -- click on other tab
            //                 * -- select input box
            //                 * -- type in same question
            //                 * -- get reply
            //                 */

            //                if (actualReply == "For the above question, you may find out from Full-time Course & Admission Advisory.")
            //                {
            //                    messageTextbox = SwitchTab();

            //                    try
            //                    {
            //                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            //                        // Goes to div.wc-message-content > div > div  // msgCount + 1 = first reply from bot, other replies will be ignored (e.g. feedback / more question with options)
            //                        msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(msgCount + 2) + "]/div[1]/div/div/div"));
            //                    }
            //                    catch
            //                    {
            //                        try
            //                        {
            //                            botWorksheet.Cells[expectedReplyRowNumber + 1][currentColumnNumber].Value2 = msgCount;
            //                            msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*")).Count;
            //                            //botWorksheet.Cells[expectedReplyRowNumber + 1 + 1][currentColumnNumber].Value2 = msgCount;

            //                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            //                            msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(msgCount) + "]/div[1]/div/div/div"));
            //                        }
            //                        catch (Exception ex)
            //                        {
            //                            botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = ex.ToString();
            //                            continue;
            //                        }
            //                    }
            //                    // compare the answers without tab, next line and return carriage
            //                    actualReply = refineReply(msgContent.GetAttribute("innerText"));
            //                    expectedReply = refineReply(botRange.Cells[answerRowNumber][currentColumnNumber].Value2);
            //                }
            //                else if (actualReply == "For non course-related queries (e.g. facilities), you may find out from General Enquiries.")
            //                {
            //                    messageTextbox = SwitchTab();

            //                    try
            //                    {
            //                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            //                        // Goes to div.wc-message-content > div > div  // msgCount + 1 = first reply from bot, other replies will be ignored (e.g. feedback / more question with options)
            //                        msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(msgCount + 2) + "]/div[1]/div/div/div"));
            //                    }
            //                    catch
            //                    {
            //                        try
            //                        {
            //                            botWorksheet.Cells[expectedReplyRowNumber + 1][currentColumnNumber].Value2 = msgCount;
            //                            msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*")).Count;
            //                            //botWorksheet.Cells[expectedReplyRowNumber + 1 + 1][currentColumnNumber].Value2 = msgCount;

            //                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            //                            msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(msgCount) + "]/div[1]/div/div/div"));
            //                        }
            //                        catch (Exception ex)
            //                        {
            //                            botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = ex.ToString();
            //                            continue;
            //                        }
            //                    }
            //                    // compare the answers without tab, next line and return carriage
            //                    actualReply = refineReply(msgContent.GetAttribute("innerText"));
            //                    expectedReply = refineReply(botRange.Cells[answerRowNumber][currentColumnNumber].Value2);
            //                }

            //                if (expectedReply == actualReply)
            //                {
            //                    botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
            //                }
            //                else
            //                {
            //                    botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";

            //                    botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;

            //                    botWorksheet.Cells[expectedReplyRowNumber][currentColumnNumber].Value2 = expectedReply;

            //                    botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = compareReplies(actualReply, expectedReply, botRange.Cells[remarksRowNumber][currentColumnNumber].Value2);
            //                }

            //                currentColumnNumber += 1;
            //            }
            //            catch (Exception ex)
            //            {
            //                Console.Write(ex);
            //                botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = ex.ToString();
            //                //botWorksheet.Cells[expectedReplyRowNumber + 1][currentColumnNumber].Value2 = msgCount + 2;

            //                //// track current message count + next question
            //                //msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div")).Count;
            //                //botWorksheet.Cells[expectedReplyRowNumber + 1 + 1][currentColumnNumber].Value2 = msgCount;

            //                //continue;
            //                break; // worksheet.UsedRange gets cells that are formatted, not just cells with value, thus causing a null exception when cells are formatted but have nothing in them
            //            }
            //        }

            //        // Set worksheet to null due to some semantic errors (COM object)
            //        //botWorksheet = null;
            //        //botRange = null;
            //    }
            //}
            //catch (Exception e)
            //{
            //    Console.Write(e);
            //}
        }

        [TearDown]
        public void CloseBrowser()
        {
            // save to new excelsheet
            int count = 1;
            while (File.Exists(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx"))
            {
                count += 1;
            }

            botWorkbook.SaveAs(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx");

            botWorkbook.Close();

            botApp.Quit();

            // release all COM object (saving RAM)
            if (botRange != null) Marshal.ReleaseComObject(botRange);
            if (botWorksheet != null) Marshal.ReleaseComObject(botWorksheet);
            if (botWorkbook != null) Marshal.ReleaseComObject(botWorkbook);
            if (botApp != null) Marshal.ReleaseComObject(botApp);

            Thread.Sleep(3000);

            driver.Quit();
        }

        private string refineReply(string reply)
        {
            reply = _trimReply(reply);

            reply = _removeTags(reply);

            reply = _removeNestedWhitespaces(reply);

            return reply;
        }

        private string _trimReply(string reply)
        {
            Regex.Replace(reply, "\u00A0", " ");

            char[] delimitersChars = { '\n', '\r' };

            string[] replies = reply.Split(delimitersChars);

            string trimmedReplies = "";

            for (int i = 0; i < replies.Length; i++)
            {
                string trimmed = replies[i].Trim();

                trimmed = _ignoreList(trimmed);

                trimmedReplies += trimmed;
            }

            Regex.Replace(trimmedReplies, @"\t|\n|\r", "");

            //trimmedReplies = _removeNestedWhitespaces(trimmedReplies.ToCharArray());

            // not working! failing to remove smart (curly) quotations
            Regex.Replace(trimmedReplies, @"’", "\'");

            Regex.Replace(trimmedReplies, "\u2018", "\'");
            Regex.Replace(trimmedReplies, "\u2019", "\'");
            Regex.Replace(trimmedReplies, "\u201b", "\'");
            Regex.Replace(trimmedReplies, "\u2032", "\'");

            return trimmedReplies;
        }

        private string _removeTags(string text)
        {
            bool isTag = false;

            string newText = "";

            char[] charArray = text.ToCharArray();

            for (int i = 0; i < charArray.Length; i++)
            {
                if (isTag)
                {
                    if (charArray[i].Equals('>'))
                    {
                        isTag = false;
                    } // else ignore
                }
                else
                {
                    if (charArray[i].Equals('<'))
                    {
                        isTag = true;
                    }
                    else
                    {
                        newText += charArray[i];
                    }
                }
            }

            return newText;
        }

        private string _ignoreList(string text)
        {
            string newText = text;

            if (!string.IsNullOrWhiteSpace(text))
            {
                char[] charArray = text.ToCharArray();

                if (char.IsDigit(charArray[0]))
                {
                    for (int i = 1; i < charArray.Length; i++)
                    {
                        if (charArray[i].Equals(')') || charArray[i].Equals('.')) // is a list
                        {
                            newText = text.Substring(i + 1).Trim();

                            break;
                        }
                        else if (!char.IsWhiteSpace(charArray[i])) // is not a list
                        {
                            break;
                        }
                    }
                }
                else if (charArray[0].Equals('-'))
                {
                    newText = text.Substring(1).Trim(); // is a list
                }
                else if (text.Contains("•"))
                {
                    for (int i = 0; i < charArray.Length; i++)
                    {
                        if (charArray[i].Equals('•')) // is a list
                        {
                            newText = text.Substring(i + 1).Trim();

                            break;
                        }
                        else if (!char.IsWhiteSpace(charArray[i])) // is not a list
                        {
                            break;
                        }
                    }
                }
            }

            return newText;
        }

        private string _removeNestedWhitespaces(string str)
        {
            //StringBuilder sb = new StringBuilder();
            //int indx = 0, length = st.Length;
            //while (indx < length)
            //{
            //    sb.Append(st[indx]);
            //    indx++;
            //    while (indx < length && st[indx] == ' ')
            //        indx++;
            //}
            //return sb.ToString();

            string[] strList = str.Split(' ');

            string newStr = "";

            foreach (string i in strList)
            {
                if (!string.IsNullOrWhiteSpace(i))
                {
                    newStr += i + " ";
                }
            }

            return newStr.Trim();
        }

        //// only called when there is a difference
        private string compareReplies(string actualReply, string expectedReply, string currentRemarks)
        {
            /* TODO:
             * - Show word before and after the difference
             * - Show which word is different (the nth word is different in actual results)
             */

            string remarks = currentRemarks + "\n\n Sng Jing Qi:";

            char[] actual = actualReply.ToCharArray();

            char[] expected = expectedReply.ToCharArray();

            if (actual.Length != expected.Length)
            {
                remarks += "\n - the length of both result are different";
            }

            // NOTE: not needed since the list format is removed when refining 
            //if (_isList(actualReply) || _isList(expectedReply))
            //{
            //    remarks += "\n - contains a list with format '#.' or '#)'";
            //}

            try
            {
                for (int i = 0; i < actual.Length; i++)
                {
                    if (actual[i] != expected[i])
                    {
                        if (expected[i] == '\'' || expected[i] == '\"') // ignore quotations
                        {
                            continue;
                        }
                        else if (expected[i] == ' ' && expected[i - 1] == ' ')
                        {
                            remarks += "\n - character at " + (i).ToString() + " contains a double space in the expected result";
                        }
                        else if (actual[i] == ' ' && actual[i - 1] == ' ')
                        {
                            remarks += "\n - character at " + (i).ToString() + " contains a double space in the actual result";
                        }
                        else
                        {
                            remarks += "\n - character at " + (i + 1).ToString() + " is different for both results";
                        }

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }

            if (remarks == currentRemarks + "\n\n Sng Jing Qi:")
            {
                remarks = "both replies are the same";
            }

            return remarks;
        }

        //private bool _isList(string text)
        //{
        //    if (!string.IsNullOrWhiteSpace(text))
        //    {
        //        char[] charArray = text.ToCharArray();

        //        if (char.IsDigit(charArray[0]))
        //        {
        //            for (int i = 1; i < charArray.Length; i++)
        //            {
        //                if (charArray[i].Equals(')') || charArray[i].Equals('.')) // is a list
        //                {
        //                    return true;
        //                }
        //                else if (!char.IsWhiteSpace(charArray[i])) // is not a list
        //                {
        //                    return false;
        //                }
        //            }
        //        }
        //        else if (charArray[0].Equals('-'))
        //        {
        //            for (int i = 1; i < charArray.Length; i++)
        //            {
        //                if (char.IsWhiteSpace(charArray[i])) // is a list
        //                {
        //                    return true;
        //                }
        //                else
        //                {
        //                    return false;
        //                }
        //            }
        //        }
        //        else if (text.Contains("•"))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //private IWebElement SwitchTab()
        //{
        //    if (currTab == "l1")
        //    {
        //        // wait for the following element is be present
        //        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //        // select general enquiries tab
        //        IWebElement generalEnquiriesTab = driver.FindElement(By.XPath(".//*[@id='l2']"));
        //        generalEnquiriesTab.Click();

        //        Thread.Sleep(500); // give animation some mercy

        //        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //        // select search textbox
        //        IWebElement messageTextbox = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[2]/div/input"));
        //        messageTextbox.Click();

        //        currTab = "l2";

        //        return messageTextbox;
        //    }
        //    else
        //    {
        //        // wait for the following element is be present
        //        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //        // select general enquiries tab
        //        IWebElement generalEnquiriesTab = driver.FindElement(By.XPath(".//*[@id='l1']"));
        //        generalEnquiriesTab.Click();

        //        Thread.Sleep(500); // give animation some mercy

        //        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //        // select search textbox
        //        IWebElement messageTextbox = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[2]/div/input"));

        //        currTab = "l1";

        //        return messageTextbox;
        //    }
        //}

        //[Test]
        //public void NYPChatBotChrome_v2()
        //{
        //    // open chatbot by clicking on img
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

        //    IWebElement NYPChatBotImg = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));

        //    NYPChatBotImg.Click();

        //    Thread.Sleep(500); // some mercy for animation, can delete


        //    // init excelsheet
        //    string currTab = "l2";

        //    List<string> whitelistWorkSheets = new List<string> { "SIT", "SEG", "SBM" };

        //    int questionRowNumber = 1;

        //    int answerRowNumber = 2; // expected reply

        //    int statusRowNumber = 7;

        //    int tab1MsgCount = 3;

        //    int tab2MsgCount = 2;

        //    // to record incorrect replies, personal use, delete later
        //    int remarksRowNumber = 6;

        //    int actualReplyRowNumber = 8;

        //    int expectedReplyRowNumber = 9;



        //    IWebElement textbox = _selectTab(currTab);

        //    for (int whitelistIndex = 0; whitelistIndex < whitelistWorkSheets.Count; whitelistIndex++)
        //    {
        //        int currentColumnNumber = 2;

        //        try
        //        {
        //            botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item(whitelistWorkSheets[whitelistIndex]);

        //            botWorksheet.Cells[statusRowNumber][1].Value2 = "Status";
        //            // personal use, delete later
        //            botWorksheet.Cells[actualReplyRowNumber][1].Value2 = "Actual reply (refined)";
        //            botWorksheet.Cells[expectedReplyRowNumber][1].Value2 = "Expected reply (refined)";
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.Write(ex); // sheet name does not exist

        //            continue; // next sheet
        //        }

        //        while (!string.IsNullOrWhiteSpace(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2) && !string.IsNullOrWhiteSpace(botWorksheet.Cells[answerRowNumber][currentColumnNumber].Value2))
        //        {
        //            // Ask question from excel sheet
        //            textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

        //            textbox.SendKeys(Keys.Enter);

        //            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //            string actualReply = "";

        //            // track current message count
        //            if (currTab == "l1")
        //            {
        //                int tempMsgCount = driver.FindElements(By.XPath("//*[@id='scrollingChat']")).Count;

        //                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //                if (tab1MsgCount == tempMsgCount)
        //                {
        //                    actualReply = driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText");
        //                }
        //                else
        //                {
        //                    // check if current element = question OR previous = feedback
        //                    string tempReply = driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText");

        //                    // find the question
        //                    while (tempReply != botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2 && tab1MsgCount <= tempMsgCount)
        //                    {
        //                        tab1MsgCount += 1;

        //                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //                        tempReply = driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText");
        //                    }

        //                    // get reply after question
        //                    tab1MsgCount += 1;

        //                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //                    actualReply = driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText");
        //                }

        //                tab1MsgCount += 2;
        //            }
        //            else if (currTab == "l2")
        //            {
        //                // **********FIND QUESTION, GET NEXT REPLY

        //                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //                int tempMsgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*[div]")).Count;

        //                if (tab2MsgCount < tempMsgCount)
        //                {
        //                    // re-ask question
        //                    tab2MsgCount = tempMsgCount;

        //                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //                    actualReply = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div")).GetAttribute("innerText");
        //                }
        //                else if (tab2MsgCount != tempMsgCount)
        //                {
        //                    try
        //                    {
        //                        // check if current element = question
        //                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //                        string tempReply = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div/div")).GetAttribute("innerText");

        //                        // find the question
        //                        while (tempReply != botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2 && tab2MsgCount <= tempMsgCount)
        //                        {
        //                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //                            tempReply = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div")).GetAttribute("innerText");

        //                            tab2MsgCount += 1;
        //                        }

        //                        // get reply after question
        //                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //                        actualReply = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div/div")).GetAttribute("innerText");
        //                    }
        //                    catch
        //                    {
        //                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

        //                        actualReply = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div")).GetAttribute("innerText");

        //                        tab2MsgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*[div]")).Count;
        //                    }
        //                }

        //                tab2MsgCount += 2;
        //                //break;
        //            }

        //            // Refine, compare and record status
        //            botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;

        //            currentColumnNumber += 1; // next question
        //        }
        //    }
        //}

        private IWebElement _selectTab(string tabID)
        {
            IWebElement messageTextbox;

            driver.SwitchTo().DefaultContent();

            // select tab
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            IWebElement NYPChatBotTab = driver.FindElement(By.XPath(".//*[@id='" + tabID + "']"));

            NYPChatBotTab.Click();

            // select search textbox
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            if (tabID == "l1")
            {
                IWebElement tab1IFrame = driver.FindElement(By.XPath("//*[@id='tab1chatbot']"));

                driver.SwitchTo().Frame(tab1IFrame);

                messageTextbox = driver.FindElement(By.XPath("//*[@id='textInput']"));
            }
            else //if (tabID == "l2")
            {
                messageTextbox = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[2]/div/input"));
            }
            //else //default
            //{
            //    messageTextbox = driver.FindElement(By.XPath("//*[@id='textInput']"));
            //}

            messageTextbox.Click();

            return messageTextbox;
        }



        //[Test]
        public void RecordSentMessage()
        {
            int tab2Counter = 2;
            int actualReplyRowNumber = 8;
            int currentColumnNumber = 7; // 502, 571

            botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item("SBM");

            // open chatbot by clicking on img
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

            IWebElement NYPChatBotImg = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));

            NYPChatBotImg.Click();

            Thread.Sleep(500); // some mercy for animation, can delete

            IWebElement textbox = _selectTab("l2");

            textbox.SendKeys(botWorksheet.Cells[1][currentColumnNumber].Value2);

            textbox.SendKeys(Keys.Enter);

            while (true)
            {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(100);

                string messageFound = _trimReply(driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2Counter + "]/div[1]/div/div")).GetAttribute("innerText"));

                if (messageFound == _trimReply(botWorksheet.Cells[1][currentColumnNumber].Value2))
                {
                    //try
                    //{
                    //    //botWorksheet.Cells[8][2].Value2 = messageFound;

                    //    tab2Counter += 1;

                    //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

                    //    string actualReply = _trimReply(driver.FindElement(By.XPath("/[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2Counter + "]/div[1]/div/div/div")).GetAttribute("innerText"));

                    //    botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;

                    //    break;
                    //}
                    //catch
                    //{
                    //    // Ask question again
                    //    textbox.SendKeys(botWorksheet.Cells[1][currentColumnNumber].Value2);

                    //    textbox.SendKeys(Keys.Enter);

                    //    continue; // search for this question
                    //}

                    botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = compareReplies(messageFound, _trimReply(botWorksheet.Cells[1][currentColumnNumber].Value2), "");
                    break;
                }
                else
                {
                    botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = compareReplies(messageFound, _trimReply(botWorksheet.Cells[1][currentColumnNumber].Value2), "");

                    //botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].value2 = messageFound;
                    break;
                    //tab2Counter += 1;
                }
            }
        }

        //[Test]
        public void NYPChatBotChrome_v3()
        {
            // open chatbot by clicking on img
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

            IWebElement NYPChatBotImg = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));

            NYPChatBotImg.Click();

            Thread.Sleep(500); // some mercy for animation, can delete


            // init excelsheet
            string currTab = "l2";

            List<string> whitelistWorkSheets = new List<string> { "SIT", "SEG", "SBM" };

            int questionRowNumber = 1;

            int answerRowNumber = 2; // expected reply

            int statusRowNumber = 7;

            int tab1MsgCount = 2;

            int tab2MsgCount = 2;

            // to record incorrect replies, personal use, delete later
            int remarksRowNumber = 6;

            int actualReplyRowNumber = 8;

            int expectedReplyRowNumber = 9;



            IWebElement textbox = _selectTab(currTab);

            for (int whitelistIndex = 0; whitelistIndex < whitelistWorkSheets.Count; whitelistIndex++)
            {
                int currentColumnNumber = 2;

                bool tabSwitched = false;

                try
                {
                    botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item(whitelistWorkSheets[whitelistIndex]);

                    botWorksheet.Cells[statusRowNumber][1].Value2 = "Status";
                    // personal use, delete later
                    botWorksheet.Cells[actualReplyRowNumber][1].Value2 = "Actual reply (refined)";
                    //botWorksheet.Cells[expectedReplyRowNumber][1].Value2 = "Expected reply (refined)";
                }
                catch (Exception ex)
                {
                    Console.Write(ex); // sheet name does not exist

                    continue; // next sheet
                }

                while (!string.IsNullOrWhiteSpace(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2) && !string.IsNullOrWhiteSpace(botWorksheet.Cells[answerRowNumber][currentColumnNumber].Value2))
                {
                    // Ask question from excel sheet
                    textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

                    textbox.SendKeys(Keys.Enter);

                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

                    string actualReply = "";

                    string messageFound = "";

                    bool shouldDelay = false;

                    // track current message count
                    if (currTab == "l1")
                    {
                        while (true)
                        {
                            //try
                            //{
                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(1);

                            messageFound = _removeNestedWhitespaces(_trimReply(driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText")));
                            //}
                            //catch
                            //{
                            //
                            //}

                            if (messageFound == _removeNestedWhitespaces(_trimReply(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2)))
                            {
                                try
                                {
                                    tab1MsgCount += 1;

                                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(12);

                                    actualReply = refineReply(driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText"));

                                    break;
                                }
                                catch
                                {
                                    //// Ask question again
                                    //textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

                                    //textbox.SendKeys(Keys.Enter);

                                    //shouldDelay = true;

                                    //continue; // search for this question


                                    //// NEXT
                                    //actualReply = "NO REPLY FOUND";

                                    //break;


                                    // ask question again at tab 2
                                    actualReply = "For non course-related queries (e.g. facilities), you may find out from General Enquiries.Useful | Not Useful | Comment";

                                    break;
                                }
                            }
                            else
                            {
                                tab1MsgCount += 1;
                            }
                        }

                        if (actualReply == "For non course-related queries (e.g. facilities), you may find out from General Enquiries.Useful | Not Useful | Comment")
                        {
                            if (tabSwitched)
                            {
                                // dead question
                                actualReply = "BULLIED";
                            }
                            else
                            {
                                currTab = "l2";

                                textbox = _selectTab(currTab);

                                tabSwitched = true;

                                continue; // ask question again at tab 2

                            }
                        }
                    }
                    else if (currTab == "l2")
                    {
                        while (true)
                        {
                            //try
                            //{
                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(1);

                            messageFound = _removeNestedWhitespaces(_trimReply(driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div")).GetAttribute("innerText")));
                            //}
                            //catch
                            //{
                            //
                            //}

                            if (messageFound == _removeNestedWhitespaces(_trimReply(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2)))
                            {
                                try
                                {
                                    tab2MsgCount += 1;

                                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(12);

                                    actualReply = refineReply(driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + tab2MsgCount + "]/div[1]/div/div/div")).GetAttribute("innerText"));

                                    break;
                                }
                                catch
                                {
                                    // Ask question again
                                    textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

                                    textbox.SendKeys(Keys.Enter);

                                    shouldDelay = true;

                                    continue; // search for this question
                                }
                            }
                            else
                            {
                                tab2MsgCount += 1;
                            }
                        }

                        if (actualReply == "For the above question, you may find out from Full-time Course & Admission Advisory.")
                        {
                            if (tabSwitched)
                            {
                                // dead question
                                actualReply = "BULLIED";
                            }
                            else
                            {
                                currTab = "l1";

                                textbox = _selectTab(currTab);

                                tabSwitched = true;

                                continue; // ask question again at tab 1
                            }
                        }
                    }


                    // Refine, compare and record status
                    botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;

                    //if (refineReply(botWorksheet.Cells[answerRowNumber][currentColumnNumber].Value2) == actualReply)
                    //{
                    //    botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
                    //}
                    //else
                    //{
                    string comparedResults = compareReplies(actualReply, refineReply(botWorksheet.Cells[answerRowNumber][currentColumnNumber].Value2), botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2);
                    if (comparedResults == "both replies are the same") // quotations ignored
                    {
                        botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
                    }
                    else
                    {
                        botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";

                        botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = comparedResults;
                    }
                    //}

                    tabSwitched = false;

                    currentColumnNumber += 1; // next question

                    if (shouldDelay)
                    {
                        Thread.Sleep(100); // for the reasked questions to spam answers
                    }
                }
            }
        }


    }
}



/* xpath to count messages 
//*[@id='BotChat']/div/div/div[1]/div[1]/div/*[div]

/* xpath to message sent 
//*[@id="BotChat"]/div/div/div[1]/div[1]/div/div[#]/div[1]/div/div/span

/* xpath to message received */
//*[@id="BotChat"]/div/div/div[1]/div[1]/div/div[#]/div[1]/div/div/div
/*
Feedback

To help me improve in answering your future question(s), please choose whether you "👍" or "👎" our answers.

Like 👍
Dislike 👎
Comment
 */
// actualReply == "For the above question, you may find out from Full-time Course & Admission Advisory." /* switch to tab 1 */ //*[@id="textInput"]
// actualReply == "For non course-related queries (e.g. facilities), you may find out from General Enquiries." /* switch to tab 2 */

/*
 // switch driver to iframe
            IWebElement nypBotIFrame = driver.FindElement(By.Id("nypBot"));

            driver.SwitchTo().Frame(nypBotIFrame);
 */
