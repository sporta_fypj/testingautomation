﻿using System;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Excel = Microsoft.Office.Interop.Excel;

namespace UnitTestBot
{
    class NYPChatBotDev1Tabs
    {
        IWebDriver driver;
        Excel.Application botApp;
        Excel.Workbook botWorkbook;
        Excel._Worksheet botWorksheet;
        Excel.Range botRange;
        readonly string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\..\..\..\..\..";

        public NYPChatBotDev1Tabs()
        {
            botApp = new Excel.Application();
            botWorkbook = botApp.Workbooks.Open(filePath + @"\03. SampleQnA\SampleQnATest");
            botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item("Sheet1");
            botRange = botWorksheet.UsedRange;
        }

        [SetUp]
        public void Startbrowser()
        {
            driver = new ChromeDriver(filePath + @"\02. Software\chromedriver_win32");
            
            driver.Url = "https://asknypadmindev.azurewebsites.net/BotWeb";

            driver.Manage().Window.Maximize();
        }

        //[Test]
        public void Test()
        {
            try
            {
                //Click on the AskNYP Chatbot image
                IWebElement nypbotImage = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));
                nypbotImage.Click();

                Thread.Sleep(600); // give animation some mercy

                // wait for the following element is be present
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                // select search textbox
                IWebElement messageTextbox = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[3]/div/input"));
                messageTextbox.Click();


                /* init excelsheet */
                int questionRowNumber = 1;
                int answerRowNumber = 2; // expected reply
                int statusRowNumber = 3;
                int actualReplyRowNumber = 4; // to record incorrect replies

                int currentColumnNumber = 2; // init with starting column
                int msgCount = 0;

                for (int index = 0; index < (botWorksheet.Columns.Count - currentColumnNumber) + 1; index++) // +1 after minus because the starting column is deleted from the total columns. could -1 then minus, but it may be a little more confusing
                {
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                    // track current message count + next question
                    msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[2]/div[1]/div/*")).Count + 1;

                    // Enter question from excelsheet into textbox
                    messageTextbox.SendKeys(Convert.ToString(botRange.Cells[questionRowNumber][currentColumnNumber].Value2));

                    // Send Question
                    messageTextbox.SendKeys(Keys.Enter);

                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                    // Goes to div.wc-message-content > div > div  // msgCount + 1 = first reply from bot, other replies will be ignored (e.g. feedback / more question with options)
                    IWebElement msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[2]/div[1]/div/div[" + Convert.ToString(msgCount + 1) + "]/div[1]/div/div/div"));


                    // compare the answers without tab, enter and return carriage
                    string actualReply = Regex.Replace(msgContent.GetAttribute("innerText"), @"\t|\n|\r", "");
                    string expectedReply = Regex.Replace(Convert.ToString(botRange.Cells[answerRowNumber][currentColumnNumber].Value2), @"\t|\n|\r", "");
                    actualReply = Regex.Replace(actualReply, "\u00A0", " ");
                    expectedReply = Regex.Replace(expectedReply, "\u00A0", " ");

                    if (expectedReply == actualReply)
                    {
                        botRange.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
                    }
                    else
                    {
                        botRange.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";

                        botRange.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;
                    }

                    currentColumnNumber += 1;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        [TearDown]
        public void CloseBrowser()
        {
            // save to new excelsheet
            int count = 1;
            while (File.Exists(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx"))
            {
                count += 1;
            }

            botWorkbook.SaveAs(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx");

            botWorkbook.Close();

            Thread.Sleep(3000);
            
            driver.Quit();
        }
    }
}