﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using Excel = Microsoft.Office.Interop.Excel;

namespace UnitTestBot
{
    class BotDemo
    {
        IWebDriver driver;
        Excel.Application botApp;
        Excel.Workbook botWorkbook;
        Excel._Worksheet botWorksheet;
        Excel.Range botRange;

        public BotDemo()
        {
            botApp = new Excel.Application();
            //botWorkbook = botApp.Workbooks.Open(@"D:\\Karen\\CloneAskNYP\\Selenium_WebDriverTest\\QnATestDatapool.xlsx");
            botWorkbook = botApp.Workbooks.Open(@"C:\Users\L33524\Documents\JingQi_baseline project\03. SampleQnA\SampleQnATest");
            botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item("Sheet1");
            botRange = botWorksheet.UsedRange;
        }

        [SetUp]
        public void Startbrowser()
        {
            //driver = new ChromeDriver("D:\\Karen\\Software");

            driver = new ChromeDriver(@"C:\Users\L33524\Documents\JingQi_baseline project\02. Software\chromedriver_win32");

            // Production URL
            driver.Url = "https://www.nyp.edu.sg";

            // UAThttps:
            //driver.Url = "https://asknypadmindev.azurewebsites.net/botwebchat";

            //driver.Manage().Window.Maximize();
        }

        //[Test]
        public void Test()
        {
            try
            {
                //Click on the AskNYP Chatbot image - ["xpath=//div[@id='botDiv']/div/img", "xpath:idRelative"],
                IWebElement nypbotImage = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));
                nypbotImage.Click();

                Thread.Sleep(600);

                // wait for a few seconds
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);


                // new chatbot
                // select general enquiries
                IWebElement geTab = driver.FindElement(By.XPath(".//*[@id='l2']"));
                geTab.Click();

                Thread.Sleep(600);

                // wait for a few seconds
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);
                
                // switch driver to iframe
                IWebElement nypBotIFrame = driver.FindElement(By.Id("nypBot"));

                driver.SwitchTo().Frame(nypBotIFrame);

                // wait for a few seconds
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);
                
                IWebElement messageTextbox = driver.FindElement(By.XPath(".//*[@id='bot']/div/div/div[3]/div/input"));
                messageTextbox.Click();

                // original chatbot
                //IWebElement messageTextbox = driver.FindElement(By.XPath(".//*[@id='BotChat']/div/div/div[3]/div/input"));
                //messageTextbox.Click();


                /* init excelsheet */
                int questionRowNumber = 1;
                int answerRowNumber = 2;
                int statusRowNumber = 3;
                int actualReplyRowNumber = 4;

                int currentColumnNumber = 2; // init with starting column

                for (int index = 0; index < (botWorksheet.Columns.Count - currentColumnNumber) + 1; index++)
                {
                    string actualReply = "";

                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                    // Enter question from excelsheet into textbox
                    messageTextbox.SendKeys(Convert.ToString(botRange.Cells[questionRowNumber][currentColumnNumber].Value2));

                    // Send Question
                    messageTextbox.SendKeys(Keys.Enter);

                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                    // TODO: dynamically grab the newest reply(s) from chatbot
                    // TODO: filter out unwanted responses (e.g. like dislike comment / options)
                    /* Get reply from all tags' innerText */
                    // new chatbot
                    IWebElement msgContent = driver.FindElement(By.XPath(".//*[@id='bot']/div/div/div[2]/div[1]/div/div[" + Convert.ToString(3 + 2 * index) + "]/div[1]/div/div/div")); // Goes to div.wc-message-content (contains svg and div tag)
                    //actualReply = getChildren(msgContent, actualReply);
                    actualReply += msgContent.GetAttribute("innerText");

                    // original chatbot
                    //IWebElement msgContent = driver.FindElement(By.XPath(".//*[@id='BotChat']/div/div/div[2]/div[1]/div/div[" + Convert.ToString(3 + 2 * index) + "]/div[1]/div/div/div")); // Goes to div.wc-message-content (contains svg and div tag)
                    //actualReply = getDescendants(msgContent, actualReply);

                    /* get reply from p tags' innerText */
                    // new chatbot
                    //IList<IWebElement> msgContent = driver.FindElements(By.XPath("//*[@id='bot']/div/div/div[2]/div[1]/div/div[" + Convert.ToString(3 + 2 * index) + "]/div[1]/div/div/div/p"));

                    // original chatbot
                    //IList<IWebElement> msgContent = driver.FindElements(By.XPath(".//*[@id='BotChat']/div/div/div[2]/div[1]/div/div[" + Convert.ToString(3 + 2 * index) + "]/div[1]/div/div/div/p"));

                    //for (int i = 0; i < msgContent.Count; i++)
                    //{
                    //    actualReply += msgContent[i].GetAttribute("innerText");
                    //}


                    // compare the answers
                    actualReply = Regex.Replace(actualReply, @"\t|\n|\r", "");
                    string expectedReply = Convert.ToString(botRange.Cells[answerRowNumber][currentColumnNumber].Value2);

                    if (expectedReply == actualReply)
                    {
                        botRange.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
                    }
                    else
                    {
                        botRange.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";

                        botRange.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;
                    }

                    currentColumnNumber += 1;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }


            /*
             * UAT - Tab2 "AskNYP"
             * //*[@id="l2"] #l2 aria-controls="asknyp"
             */

            //IWebElement nypTab = driver.FindElement(By.XPath(".//*[@id='botDiv']//div[2]/label[@for='tab2']"));
            //nypTab.Click();

            //// wait for a seconds
            //Task.Delay(1000).Wait();

            //IWebElement messageTextbox = driver.FindElement(By.XPath("./[@id='botDiv']/div/div/div/div[3]/div/input[@value='']"));
            //messageTextbox.Click();

            //Task.Delay(1000).Wait();
        }

        [TearDown]
        public void CloseBrowser()
        {
            // save to new excelsheet
            botWorkbook.SaveAs(@"C:\Users\L33524\Documents\JingQi_baseline project\03. SampleQnA\SampleQnATestResults.xlsx");

            botWorkbook.Close();

            Thread.Sleep(3000);

            driver.Close();
        }

        /* DELETE: attempt dynamic grabbing of innerText from reply(s) */
        public string getChildren(IWebElement element, string masterStr)
        {
            try
            {
                // instead of innerText, should get data/nodevalue?
                string text = element.GetAttribute("innerText");

                if (!string.IsNullOrWhiteSpace(text) && text != @"\n")
                {
                    masterStr += text;
                }

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(3000);

                IList<IWebElement> descendants = element.FindElements(By.XPath("*"));

                for (int i = 0; i < descendants.Count; i++)
                {
                    if (descendants[i] != element)
                    {
                        masterStr += getChildren(descendants[i], masterStr);
                    }
                }
            }
            catch (Exception ex)
            {
                // No more child

                Console.Write(ex);
            }

            return masterStr;
        }
    }
}
 