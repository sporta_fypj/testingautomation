﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Excel = Microsoft.Office.Interop.Excel;

namespace UnitTestBot
{
    class BackUp4
    {
        IWebDriver driver;
        IWebElement msgContent;
        Excel.Application botApp;
        Excel.Workbook botWorkbook;
        Excel._Worksheet botWorksheet;
        Excel.Range botRange;
        readonly string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\..\..\..\..\..";

        public BackUp4()
        {
            botApp = new Excel.Application();
            //botWorkbook = botApp.Workbooks.Open(@"C:\Users\L33524\Documents\JingQi_baseline project\03. SampleQnA\SampleQnATest");
            //botWorkbook = botApp.Workbooks.Open(filePath + @"\03. SampleQnA\QnABankMutlipleSheets");
            botWorkbook = botApp.Workbooks.Open(filePath + @"\03. SampleQnA\QnABankForFYPJstudent");
        }

        [SetUp]
        public void Startbrowser()
        {
            driver = new ChromeDriver(filePath + @"\02. Software\chromedriver_win32");

            driver.Url = "https://asknypadmindev.azurewebsites.net/botwebchat";

            //driver.Manage().Window.Maximize();
        }

        //[Test]
        public void Test()
        {
            try
            {
                //Click on the AskNYP Chatbot image
                IWebElement nypbotImage = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));
                nypbotImage.Click();

                Thread.Sleep(600); // give animation some mercy

                // wait for the following element is be present
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);


                // select general enquiries tab
                IWebElement generalEnquiriesTab = driver.FindElement(By.XPath(".//*[@id='l2']"));
                generalEnquiriesTab.Click();

                Thread.Sleep(500); // give animation some mercy

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

                // select search textbox
                IWebElement messageTextbox = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[2]/div/input"));
                messageTextbox.Click();


                /* init excelsheet */
                List<string> whitelistWorkSheets = new List<string> { "SIT", "SEG", "SBM" };

                int questionRowNumber = 1;
                int answerRowNumber = 2; // expected reply
                int remarksRowNumber = 6;
                int statusRowNumber = 7;
                int actualReplyRowNumber = 8; // to record incorrect replies
                int expectedReplyRowNumber = 9;

                for (int whitelistIndex = 0; whitelistIndex < whitelistWorkSheets.Count; whitelistIndex++)
                {
                    int currentColumnNumber = 2; // reset starting column

                    try
                    {
                        botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item(whitelistWorkSheets[whitelistIndex]);
                        botRange = botWorksheet.UsedRange;

                        botWorksheet.Cells[statusRowNumber][1].Value2 = "Status";
                        botWorksheet.Cells[actualReplyRowNumber][1].Value2 = "Actual reply (refined)";
                        botWorksheet.Cells[expectedReplyRowNumber][1].Value2 = "Expected reply (refined)";
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex); // sheet name does not exist

                        continue; // next sheet
                    }

                    int msgCount = 0;

                    // +1 after minus because the starting column is deleted from the total columns. could -1 then minus, but it may be a little more confusing
                    int totalQuestions = botWorksheet.Columns.Count - currentColumnNumber + 1;

                    for (int index = 0; index < totalQuestions; index++)
                    {
                        /* TODO:
                         * - Don't use count to find messages
                         * -- get last msg? (if chat bot have not reply, check if the msg bubble is empty or question asked. if msg is feedback... black list?)
                         * -- save a count and get msg count to compare and do some math? (compare previous count and current count? )
                         */
                        try
                        {
                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

                            // Enter question from excelsheet into textbox
                            messageTextbox.SendKeys(Convert.ToString(botRange.Cells[questionRowNumber][currentColumnNumber].Value2));


                            Thread.Sleep(300); // slight delay incase feedback message shows

                            // track current message count + next question
                            msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*[div]")).Count + 1;

                            // Send Question
                            messageTextbox.SendKeys(Keys.Enter);

                            /* TODO:
                             * - Click retry button?
                             * - Resend question if reply not found?
                             */
                            try
                            {
                                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

                                // Goes to div.wc-message-content > div > div  // msgCount + 1 = first reply from bot, other replies will be ignored (e.g. feedback / more question with options)
                                msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(msgCount + 1) + "]/div[1]/div/div/div"));
                            }
                            catch (Exception ex)
                            {
                                msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*")).Count;
                                botWorksheet.Cells[expectedReplyRowNumber + 1 + 1][currentColumnNumber].Value2 = msgCount;

                                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                                msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(msgCount) + "]/div[1]/div/div/div"));
                            }


                            // compare the answers without tab, next line and return carriage
                            string actualReply = refineReply(msgContent.GetAttribute("innerText"));
                            string expectedReply = refineReply(botRange.Cells[answerRowNumber][currentColumnNumber].Value2);

                            //string actualReply = Regex.Replace(Regex.Replace(msgContent.GetAttribute("innerText"), "\u00A0", " "), @"\t|\n|\r", "");
                            //string expectedReply = Regex.Replace(Regex.Replace(botRange.Cells[answerRowNumber][currentColumnNumber].Value2, "\u00A0", " "), @"\t|\n|\r", "");

                            botWorksheet.Cells[expectedReplyRowNumber + 1][currentColumnNumber].Value2 = msgCount + 1;
                            msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*")).Count;
                            botWorksheet.Cells[expectedReplyRowNumber + 1 + 1][currentColumnNumber].Value2 = msgCount;

                            /* TODO: 
                             * - Check if result is 'For the above question, you may find out from Full-time Course & Admission Advisory.'
                             * -- click on other tab
                             * -- select input box
                             * -- type in same question
                             * -- get reply
                             */

                            if (expectedReply == actualReply)
                            {
                                botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
                            }
                            else
                            {
                                botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";

                                botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;

                                botWorksheet.Cells[expectedReplyRowNumber][currentColumnNumber].Value2 = expectedReply;

                                botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = compareReplies(actualReply, expectedReply, botRange.Cells[remarksRowNumber][currentColumnNumber].Value2);
                            }

                            currentColumnNumber += 1;
                        }
                        catch (Exception ex)
                        {
                            Console.Write(ex);
                            botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = ex.ToString();
                            botWorksheet.Cells[expectedReplyRowNumber + 1][currentColumnNumber].Value2 = msgCount + 1;

                            // track current message count + next question
                            msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div")).Count;
                            botWorksheet.Cells[expectedReplyRowNumber + 1 + 1][currentColumnNumber].Value2 = msgCount;

                            //continue;
                            break; // worksheet.UsedRange gets cells that are formatted, not just cells with value, thus causing a null exception when cells are formatted but have nothing in them
                        }
                    }

                    // Set worksheet to null due to some semantic errors (COM object)
                    //botWorksheet = null;
                    //botRange = null;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        [TearDown]
        public void CloseBrowser()
        {
            // save to new excelsheet
            int count = 1;
            while (File.Exists(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx"))
            {
                count += 1;
            }

            botWorkbook.SaveAs(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx");

            botWorkbook.Close();

            botApp.Quit();

            // release all COM object (saving RAM)
            if (botRange != null) Marshal.ReleaseComObject(botRange);
            if (botWorksheet != null) Marshal.ReleaseComObject(botWorksheet);
            if (botWorkbook != null) Marshal.ReleaseComObject(botWorkbook);
            if (botApp != null) Marshal.ReleaseComObject(botApp);

            Thread.Sleep(3000);

            driver.Quit();
        }

        private string refineReply(string reply)
        {
            reply = _trimReply(reply);

            reply = _removeTags(reply);

            return reply;
        }

        private string _trimReply(string reply)
        {
            Regex.Replace(reply, "\u00A0", " ");

            char[] delimitersChars = { '\n', '\r' };

            string[] replies = reply.Split(delimitersChars);

            string trimmedReplies = "";

            for (int i = 0; i < replies.Length; i++)
            {
                string trimmed = replies[i].Trim();

                trimmed = _ignoreList(trimmed);

                trimmedReplies += trimmed;
            }

            Regex.Replace(trimmedReplies, @"\t|\n|\r", "");

            // not working! failing to remove smart (curly) quotations
            //Regex.Replace(trimmedReplies, "\’", "\'");

            Regex.Replace(trimmedReplies, "\u2018", "\'");
            Regex.Replace(trimmedReplies, "\u2019", "\'");
            Regex.Replace(trimmedReplies, "\u201b", "\'");
            Regex.Replace(trimmedReplies, "\u2032", "\'");

            return trimmedReplies;
        }

        private string _removeTags(string text)
        {
            bool isTag = false;

            string newText = "";

            char[] charArray = text.ToCharArray();

            for (int i = 0; i < charArray.Length; i++)
            {
                if (isTag)
                {
                    if (charArray[i].Equals('>'))
                    {
                        isTag = false;
                    } // else ignore
                }
                else
                {
                    if (charArray[i].Equals('<'))
                    {
                        isTag = true;
                    }
                    else
                    {
                        newText += charArray[i];
                    }
                }
            }

            return newText;
        }

        private string _ignoreList(string text)
        {
            string newText = text;

            if (!string.IsNullOrWhiteSpace(text))
            {
                char[] charArray = text.ToCharArray();

                if (char.IsDigit(charArray[0]))
                {
                    for (int i = 1; i < charArray.Length; i++)
                    {
                        if (charArray[i].Equals(')') || charArray[i].Equals('.')) // is a list
                        {
                            newText = text.Substring(i + 1).Trim();

                            break;
                        }
                        else if (!char.IsWhiteSpace(charArray[i])) // is not a list
                        {
                            break;
                        }
                    }
                }
            }

            return newText;
        }

        // only called when there is a difference
        private string compareReplies(string actualReply, string expectedReply, string currentRemarks)
        {
            /* TODO:
             * - Show word before and after the difference
             * - Show which word is different (the nth word is different in actual results)
             */

            string remarks = currentRemarks + "\n\n Sng Jing Qi:";

            char[] actual = actualReply.ToCharArray();

            char[] expected = expectedReply.ToCharArray();

            if (actual.Length != expected.Length)
            {
                remarks += "\n - the length of both result are different";
            }

            // NOTE: not needed since the list format is removed at the start
            //if (_isList(actualReply) || _isList(expectedReply))
            //{
            //    remarks += "\n - contains a list with format '#.' or '#)'";
            //}

            try
            {
                for (int i = 0; i < actual.Length; i++)
                {
                    if (actual[i] != expected[i])
                    {
                        if (expected[i] == ' ' && expected[i - 1] == ' ')
                        {
                            remarks += "\n - character at " + (i).ToString() + " contains a double space in the expected result";
                        }
                        else if (actual[i] == ' ' && actual[i - 1] == ' ')
                        {
                            remarks += "\n - character at " + (i).ToString() + " contains a double space in the actual result";
                        }
                        else
                        {
                            remarks += "\n - character at " + (i + 1).ToString() + " is different for both results";
                        }

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }

            return remarks;
        }

        private bool _isList(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                char[] charArray = text.ToCharArray();

                if (char.IsDigit(charArray[0]))
                {
                    for (int i = 1; i < charArray.Length; i++)
                    {
                        if (charArray[i].Equals(')') || charArray[i].Equals('.')) // is a list
                        {
                            return true;
                        }
                        else if (!char.IsWhiteSpace(charArray[i])) // is not a list
                        {
                            return false;
                        }
                    }
                }
            }
            return false;
        }
    }
}