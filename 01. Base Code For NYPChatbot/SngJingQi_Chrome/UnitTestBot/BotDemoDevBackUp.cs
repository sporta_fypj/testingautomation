﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using Excel = Microsoft.Office.Interop.Excel;

namespace UnitTestBot
{
    class BotDemoDevBackUp
    {
        IWebDriver driver;
        Excel.Application botApp;
        Excel.Workbook botWorkbook;
        Excel._Worksheet botWorksheet;
        Excel.Range botRange;

        public BotDemoDevBackUp()
        {
            botApp = new Excel.Application();
            botWorkbook = botApp.Workbooks.Open(@"C:\Users\L33524\Documents\JingQi_baseline project\03. SampleQnA\SampleQnATest");
            botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item("Sheet1");
            botRange = botWorksheet.UsedRange;
        }

        [SetUp]
        public void Startbrowser()
        {
            driver = new ChromeDriver(@"C:\Users\L33524\Documents\JingQi_baseline project\02. Software\chromedriver_win32");

            driver.Url = "https://asknypadmindev.azurewebsites.net/botwebchat";

            //driver.Manage().Window.Maximize();
        }

        //[Test]
        public void Test()
        {
            try
            {
                //Click on the AskNYP Chatbot image
                IWebElement nypbotImage = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));
                nypbotImage.Click();

                Thread.Sleep(600); // give animation some mercy

                // wait for the following element is be present
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);


                // select general enquiries tab
                IWebElement generalEnquiriesTab = driver.FindElement(By.XPath(".//*[@id='l2']"));
                generalEnquiriesTab.Click();

                Thread.Sleep(600); // give animation some mercy

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                // select search textbox
                IWebElement messageTextbox = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[2]/div/input"));
                messageTextbox.Click();


                /* init excelsheet */ // Possible Additions: find keywords and init (not needed if everyone follow the same format)
                int questionRowNumber = 1;
                int answerRowNumber = 2; // expected reply
                int statusRowNumber = 3;
                int actualReplyRowNumber = 4; // to record incorrect replies

                int currentColumnNumber = 2; // init with starting column

                for (int index = 0; index < (botWorksheet.Columns.Count - currentColumnNumber) + 1; index++) // +1 after minus because the starting column is deleted from the total columns. could -1 then minus, but it may be a little more confusing
                {
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                    // Enter question from excelsheet into textbox
                    messageTextbox.SendKeys(Convert.ToString(botRange.Cells[questionRowNumber][currentColumnNumber].Value2));

                    // Send Question
                    messageTextbox.SendKeys(Keys.Enter);

                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                    // In Progress: dynamically grab the newest reply(s) from chatbot
                    // TODO: filter out unwanted responses (e.g. like dislike comment / options)

                    // Get reply from all tags' innerText
                    IWebElement msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(3 + 2 * index) + "]/div[1]/div/div/div")); // Goes to div.wc-message-content > div > div


                    // compare the answers
                    string actualReply = Regex.Replace(msgContent.GetAttribute("innerText"), @"\t|\n|\r", "");
                    string expectedReply = Regex.Replace(Convert.ToString(botRange.Cells[answerRowNumber][currentColumnNumber].Value2), @"\t|\n|\r", "");

                    if (expectedReply == actualReply)
                    {
                        botRange.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
                    }
                    else
                    {
                        botRange.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";

                        botRange.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;
                    }

                    currentColumnNumber += 1;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        [TearDown]
        public void CloseBrowser()
        {
            //
            // save to new excelsheet
            botWorkbook.SaveAs(@"C:\Users\L33524\Documents\JingQi_baseline project\03. SampleQnA\SampleQnATestResults.xlsx");

            botWorkbook.Close();

            Thread.Sleep(3000);

            driver.Close();
        }
    }
}
