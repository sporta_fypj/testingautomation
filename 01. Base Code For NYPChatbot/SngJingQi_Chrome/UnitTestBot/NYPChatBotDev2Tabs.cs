﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Excel = Microsoft.Office.Interop.Excel;

namespace UnitTestBot
{
    class NYPChatBotDev2Tabs
    {
        IWebDriver driver;
        Excel.Application botApp;
        Excel.Workbook botWorkbook;
        Excel._Worksheet botWorksheet;
        readonly string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\..\..\..\..\..";


        // init (change according to excelsheet)
        List<string> whitelistWorkSheets = new List<string> { "SIT" };

        readonly int questionRowNumber = 1;

        readonly int answerRowNumber = 2; // expected reply

        readonly int statusRowNumber = 7;


        // development mode
        /*  
         *  Set to false in working product, saves to original excelsheet
         *  Set to true to view actual reply and expected reply, saves to new excelsheet
         */
        readonly bool devMode = true;

        // for devMode = true
        readonly int remarksRowNumber = 6;

        readonly int actualReplyRowNumber = 8;

        readonly int expectedReplyRowNumber = 9;
        

        [SetUp]
        public void Startbrowser()
        {
            botApp = new Excel.Application();

            /* !! Please change if using different Excel spreadsheet !! */
            botWorkbook = botApp.Workbooks.Open(filePath + @"\03. SampleQnA\SITQnAForFYPJ"); 

            driver = new ChromeDriver()
            {
                Url = "https://asknypadmindev.azurewebsites.net/botmain"
            };

            driver.Manage().Window.Maximize();
        }

        [Test]
        public void NYPChatBotChrome()
        {
            // open chatbot by clicking on img
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

            IWebElement NYPChatBotImg = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));

            NYPChatBotImg.Click();

            Thread.Sleep(500); // some mercy for animation, can delete

            // default msg count, do not change
            int tab1MsgCount = 2;

            int tab2MsgCount = 2;


            // select General Enquiries tab as starting tab (can set default as "l1")
            string currTab = "l2";

            IWebElement textbox = SelectTab(currTab);


            // Loop through worksheets
            for (int whitelistIndex = 0; whitelistIndex < whitelistWorkSheets.Count; whitelistIndex++)
            {
                int currentColumnNumber = 2;

                bool tabSwitched = false;

                try // get worksheet and init columns
                {
                    botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item(whitelistWorkSheets[whitelistIndex]);

                    botWorksheet.Cells[statusRowNumber][1].Value2 = "Status";

                    if (devMode)
                    {
                        botWorksheet.Cells[actualReplyRowNumber][1].Value2 = "Actual reply (refined)";
                        botWorksheet.Cells[expectedReplyRowNumber][1].Value2 = "Expected reply (refined)";
                    }
                }
                catch (Exception ex)
                {
                    Console.Write(ex); // sheet name does not exist

                    continue; // next sheet
                }

                // run if there is a question and answer
                while (!string.IsNullOrWhiteSpace(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2) && !string.IsNullOrWhiteSpace(botWorksheet.Cells[answerRowNumber][currentColumnNumber].Value2))
                {
                    // Enter question from excel sheet
                    textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

                    textbox.SendKeys(Keys.Enter);

                    string actualReply = "";

                    string messageFound = "";

                    bool shouldDelay = false;


                    // Find question chat bubble and get next reply
                    if (currTab == "l1") // Full-time Course & Admission Advisory tab
                    {
                        // find question
                        while (true)
                        {
                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(1);

                            messageFound = RefineMsg(driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText"));

                            if (messageFound == RefineMsg(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2))
                            {
                                try // question found
                                {
                                    // get next reply
                                    tab1MsgCount += 1;

                                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(12);

                                    actualReply = RefineReply(driver.FindElement(By.XPath("//*[@id='scrollingChat']/div[" + tab1MsgCount + "]/div/div")).GetAttribute("innerText"));

                                    break; // exit find question algorithm (while loop)
                                }
                                catch // question failed to send
                                {
                                    //// Ask question again
                                    //textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

                                    //textbox.SendKeys(Keys.Enter);

                                    //shouldDelay = true;

                                    //continue; // search for this question


                                    //// NEXT
                                    //actualReply = "NO REPLY FOUND";

                                    //break;


                                    // ask question again at tab 2
                                    actualReply = "For non course-related queries (e.g. facilities), you may find out from General Enquiries. Useful | Not Useful | Comment";

                                    break; // exit find question algorithm (while loop)
                                }
                            }
                            else // question not found, get next msg
                            {
                                tab1MsgCount += 1;
                            }
                        }

                        // find keyword to switch tab
                        if (actualReply == "For non course-related queries (e.g. facilities), you may find out from General Enquiries. Useful | Not Useful | Comment")
                        {
                            if (tabSwitched) // same question had been entered in the other tab, no reply found
                            {
                                actualReply = "No response";
                            }
                            else // switch tab and enter question again
                            {
                                currTab = "l2";

                                textbox = SelectTab(currTab);

                                tabSwitched = true;

                                continue; // run code checking question and answer exist, also skips the "Refine, compare and update status" code

                            }
                        }
                    }
                    else if (currTab == "l2") // General Enquiries tab
                    {
                        // find question
                        while (true)
                        {
                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(1);

                            messageFound = RefineMsg(driver.FindElement(By.XPath("//*[@id='bot']/div/div/div[2]/div/div/div[" + tab2MsgCount + "]/div/div/div")).GetAttribute("innerText"));

                            if (messageFound == RefineMsg(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2))
                            {
                                try // question found
                                {
                                    // get next reply
                                    tab2MsgCount += 1;

                                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(12);

                                    actualReply = RefineReply(driver.FindElement(By.XPath("//*[@id='bot']/div/div/div[2]/div/div/div[" + tab2MsgCount + "]/div/div/div/div")).GetAttribute("innerText"));

                                    // check if it is the feedback message and get reply after feedback message (happens once in a blue moon where the question is asked before the feedback msg is received)
                                    if (actualReply == "FeedbackTo help me improve in answering your future question(s), please choose whether you \"👍\" or \"👎\" our answers.Like 👍Dislike 👎Comment")
                                    {
                                        tab2MsgCount += 1;

                                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(12);

                                        actualReply = RefineReply(driver.FindElement(By.XPath("//*[@id='bot']/div/div/div[2]/div/div/div[" + tab2MsgCount + "]/div/div/div/div")).GetAttribute("innerText"));
                                    }

                                    break; // exit find question algorithm (while loop)
                                }
                                catch // question failed to send
                                {
                                    // Ask question again
                                    textbox.SendKeys(botWorksheet.Cells[questionRowNumber][currentColumnNumber].Value2);

                                    textbox.SendKeys(Keys.Enter);

                                    shouldDelay = true;

                                    continue; // search for this question
                                }
                            }
                            else // question not found, get next msg
                            {
                                tab2MsgCount += 1;
                            }
                        }

                        // find keyword to switch tab
                        if (actualReply == "For the above question, you may find out from Full-time Course & Admission Advisory.")
                        {
                            if (tabSwitched) // same question had been entered in the other tab, no reply found
                            {
                                actualReply = "No response";
                            }
                            /* un-highlight this only if tab 1 is working. 
                            else // switch tab and enter question again
                            {
                                currTab = "l1";

                                textbox = SelectTab(currTab);

                                tabSwitched = true;

                                continue; // run code checking question and answer exist, also skips the "Refine, compare and update status" code
                            }
                            */
                        }
                    }


                    
                    // Refine, compare and update status
                    string expectedReply = RefineReply(botWorksheet.Cells[answerRowNumber][currentColumnNumber].Value2);

                    if (devMode)
                    {
                        botWorksheet.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;

                        botWorksheet.Cells[expectedReplyRowNumber][currentColumnNumber].Value2 = expectedReply;
                    }

                    if (actualReply == "No response")
                    {
                        botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect, No response";
                    }
                    else
                    {
                        // compare results
                        string comparedResults = CompareReplies(actualReply, expectedReply, botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2);

                        if (comparedResults == "both replies are the same")
                        {
                            botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
                        }
                        else
                        {
                            botWorksheet.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";

                            if (devMode) // remarks
                            {
                                botWorksheet.Cells[remarksRowNumber][currentColumnNumber].Value2 = comparedResults;
                            }
                        }
                    }

                    tabSwitched = false; // reset var

                    if (shouldDelay) // question failed to send
                    {
                        Thread.Sleep(1000); // for the reasked questions to spam answers
                    }

                    currentColumnNumber += 1; // next question
                }
            }
        }

        [TearDown]
        public void CloseBrowser()
        {
            if (devMode)
            {
                // create folder
                if (!Directory.Exists(filePath + @"\TestResults"))
                {
                    Directory.CreateDirectory(filePath + @"\TestResults");
                }

                // save to new excelsheet
                int count = 1;

                while (File.Exists(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx"))
                {
                    count += 1;
                }

                botWorkbook.SaveAs(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx");
            }
            else
            {
                // save in same excel
                botWorkbook.Save();
            }

            botWorkbook.Close();

            botApp.Quit();

            // release all COM object (Close Excel properly)
            if (botWorksheet != null) Marshal.ReleaseComObject(botWorksheet);
            if (botWorkbook != null) Marshal.ReleaseComObject(botWorkbook);
            if (botApp != null) Marshal.ReleaseComObject(botApp);

            Thread.Sleep(1000);

            driver.Quit(); // End session
        }


        private IWebElement SelectTab(string tabID)
        {
            IWebElement messageTextbox;

            driver.SwitchTo().DefaultContent();

            // select tab
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            IWebElement NYPChatBotTab = driver.FindElement(By.XPath(".//*[@id='l2']"));

            //use this only when tab 1 is working. 
          //  IWebElement NYPChatBotTab = driver.FindElement(By.XPath(".//*[@id='" + tabID + "']"));

            NYPChatBotTab.Click();

            // select search textbox
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            if (tabID == "l1")
            {
                IWebElement tab1IFrame = driver.FindElement(By.XPath("//*[@id='polyBot']"));

                driver.SwitchTo().Frame(tab1IFrame);

                messageTextbox = driver.FindElement(By.XPath("//*[@id='textInput']"));
            }
            else //if (tabID == "l2")
            {
                IWebElement tab2IFrame = driver.FindElement(By.XPath("//*[@id='nypBot']"));
                driver.SwitchTo().Frame(tab2IFrame);
                messageTextbox = driver.FindElement(By.XPath("//*[@id='bot']/div/div/div[3]/div/input"));
            }
            //else /* default */
            //{
            //    messageTextbox = driver.FindElement(By.XPath("//*[@id='textInput']"));
            //}

            messageTextbox.Click();

            return messageTextbox;
        }


        private string RefineMsg(string msg)
        {
            msg = _trimReply(msg);

            msg = _removeNestedWhitespaces(msg);

            return msg;
        }

        private string RefineReply(string reply)
        {
            reply = _trimReply(reply);

            reply = _removeTags(reply);

            reply = _removeNestedWhitespaces(reply);

            return reply;
        }

        private string _trimReply(string reply)
        {
            Regex.Replace(reply, "\u00A0", " "); // special space

            Regex.Replace(reply, " ", " "); // special space

            char[] delimitersChars = { '\n', '\r' };

            string[] replies = reply.Split(delimitersChars);

            string trimmedReplies = "";

            for (int i = 0; i < replies.Length; i++)
            {
                trimmedReplies += _ignoreList(replies[i]) + " ";
            }

            Regex.Replace(trimmedReplies, @"\t|\n|\r", " ");

            // not working! failing to remove smart (curly) quotations
            Regex.Replace(trimmedReplies, @"’", "\'");
            Regex.Replace(trimmedReplies, "\u2018", "\'");
            Regex.Replace(trimmedReplies, "\u2019", "\'");
            Regex.Replace(trimmedReplies, "\u201b", "\'");
            Regex.Replace(trimmedReplies, "\u2032", "\'");

            return trimmedReplies;
        }

        private string _ignoreList(string text)
        {
            string newText = text;

            if (!string.IsNullOrWhiteSpace(text))
            {
                char[] charArray = text.ToCharArray();

                if (char.IsDigit(charArray[0]))
                {
                    for (int i = 1; i < charArray.Length; i++)
                    {
                        if (charArray[i].Equals(')') || charArray[i].Equals('.')) // is a list
                        {
                            newText = text.Substring(i + 1);

                            break;
                        }
                        else if (!char.IsWhiteSpace(charArray[i])) // is not a list
                        {
                            break;
                        }
                    }
                }
                else if (charArray[0].Equals('-'))
                {
                    newText = text.Substring(1); // is a list
                }
                else if (text.Contains("•"))
                {
                    for (int i = 0; i < charArray.Length; i++)
                    {
                        if (charArray[i].Equals('•')) // is a list
                        {
                            newText = text.Substring(i + 1);

                            break;
                        }
                        else if (!char.IsWhiteSpace(charArray[i])) // is not a list
                        {
                            break;
                        }
                    }
                }
            }

            return newText;
        }

        private string _removeTags(string text) // Can't use innerHTML due to constrain where questions and answers cannot contain '<' as it will break everything
        {
            bool isTag = false;

            string newText = "";

            char[] charArray = text.ToCharArray();

            for (int i = 0; i < charArray.Length; i++)
            {
                if (isTag)
                {
                    if (charArray[i].Equals('>'))
                    {
                        isTag = false;
                    }
                    //else  /* ignore */
                    //{
                    //    continue;
                    //}
                }
                else
                {
                    if (charArray[i].Equals('<'))
                    {
                        isTag = true;
                    }
                    else
                    {
                        newText += charArray[i];
                    }
                }
            }

            return newText;
        }

        private string _removeNestedWhitespaces(string text) // remove all double space in the middle of sentences
        {
            char[] delimitersChars = { ' ', ' ' }; // space and special space (for some reason, some are undetected the first time)

            string[] strArray = text.Split(delimitersChars);

            string newText = "";

            foreach (string i in strArray)
            {
                if (!string.IsNullOrWhiteSpace(i))
                {
                    newText += i + " ";
                }
            }

            return newText.Trim(); // .Trim() to remove space at the end
        }


        // called to check if replies (refined) are the same, also used to dictate which character(s) to ignore
        private string CompareReplies(string actualReply, string expectedReply, string currentRemarks)
        {
            ///* TODO:
            // * - Show word before and after the difference
            // * - Show which word is different (the nth word is different in actual results)
            // */
            
            string remarks = currentRemarks + "\n\n Sng Jing Qi:";

            char[] actual = actualReply.ToCharArray();

            char[] expected = expectedReply.ToCharArray();

            if (actual.Length != expected.Length)
            {
                remarks += "\n - the length of both result are different";
            }

            try
            {
                for (int i = 0; i < actual.Length; i++)
                {
                    if (actual[i] != expected[i])
                    {
                        if (expected[i] == '\'' || expected[i] == '\"') // ignore quotations
                        {
                            continue;
                        }
                        else if (expected[i] == ' ' && expected[i - 1] == ' ')
                        {
                            remarks += "\n - character at " + (i).ToString() + " contains a double space in the expected result";
                        }
                        else if (actual[i] == ' ' && actual[i - 1] == ' ')
                        {
                            remarks += "\n - character at " + (i).ToString() + " contains a double space in the actual result";
                        }
                        else
                        {
                            remarks += "\n - character at " + (i + 1).ToString() + " is different for both results";
                        }

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }

            if (remarks == currentRemarks + "\n\n Sng Jing Qi:")
            {
                remarks = "both replies are the same";
            }

            return remarks;



            /* v2 */
            //string remarks = currentRemarks + "\n\n Sng Jing Qi:";

            //string[] actual = actualReply.Split(' ');

            //string[] expected = expectedReply.Split(' ');

            //if (actual.Length != expected.Length)
            //{
            //    remarks += "\n - the number of words for both result are different";
            //}

            //try
            //{
            //    for (int i = 0; i < actual.Length; i++)
            //    {
            //        if (actual[i] != expected[i])
            //        {
            //            if (expected[i] == "\'" || expected[i] == "\"") // ignore quotations
            //            {
            //                continue;
            //            }
            //            else
            //            {
            //                remarks += "\n - word at " + (i + 1).ToString() + " position is different for both results";

            //                char[] actualWord = actual[i].ToCharArray();

            //                char[] expectedWord = expected[i].ToCharArray();

            //                for (int charI = 0; charI < actualWord.Length; charI++)
            //                {
            //                    if (actualWord[i] != expectedWord[i])
            //                    {
            //                        remarks += "\n\t - character at " + (i + 1).ToString() + " position is different for both results";

            //                        break;
            //                    }
            //                }

            //                break;
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex) // out of range for expected (expected.Length < actual.Length)
            //{
            //    Console.Write(ex);
            //}

            //if (remarks == currentRemarks + "\n\n Sng Jing Qi:")
            //{
            //    remarks = "both replies are the same";
            //}

            //return remarks;
        }
    }
}



/* xpath to count messages */
//*[@id='BotChat']/div/div/div[1]/div[1]/div/*[div]

/* xpath to message sent (nyp chatbot) */
//*[@id="BotChat"]/div/div/div[1]/div[1]/div/div[#]/div[1]/div/div/span

/* xpath to message received (nyp chatbot) */
//*[@id="BotChat"]/div/div/div[1]/div[1]/div/div[#]/div[1]/div/div/div

/* feedback message */
// FeedbackTo help me improve in answering your future question(s), please choose whether you "👍" or "👎" our answers.Like 👍Dislike 👎Comment
/*
Feedback

To help me improve in answering your future question(s), please choose whether you "👍" or "👎" our answers.

Like 👍
Dislike 👎
Comment
 */

/* message to trigger tab switch */
// actualReply == "For the above question, you may find out from Full-time Course & Admission Advisory." /* switch to tab 1 ( //*[@id="textInput"] ) */
// actualReply == "For non course-related queries (e.g. facilities), you may find out from General Enquiries." /* switch to tab 2 */

/* e.g. for iframe */
/*
 // switch driver to iframe
            IWebElement nypBotIFrame = driver.FindElement(By.Id("nypBot"));

            driver.SwitchTo().Frame(nypBotIFrame);
 */

// devMode = false; before pushing to tfs