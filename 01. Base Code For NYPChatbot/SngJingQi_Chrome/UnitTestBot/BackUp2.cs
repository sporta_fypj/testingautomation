﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Excel = Microsoft.Office.Interop.Excel;

namespace UnitTestBot
{
    class Backup2
    {
        IWebDriver driver;
        Excel.Application botApp;
        Excel.Workbook botWorkbook;
        Excel._Worksheet botWorksheet;
        Excel.Range botRange;
        readonly string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\..\..\..\..\..";

        public Backup2()
        {
            botApp = new Excel.Application();
            //botWorkbook = botApp.Workbooks.Open(@"C:\Users\L33524\Documents\JingQi_baseline project\03. SampleQnA\SampleQnATest");
            botWorkbook = botApp.Workbooks.Open(filePath + @"\03. SampleQnA\QnABankMutlipleSheets");
        }

        [SetUp]
        public void Startbrowser()
        {
            driver = new ChromeDriver(filePath + @"\02. Software\chromedriver_win32");

            driver.Url = "https://asknypadmindev.azurewebsites.net/botwebchat";

            driver.Manage().Window.Maximize();
        }

        //[Test]
        public void Test()
        {
            try
            {
                //Click on the AskNYP Chatbot image
                IWebElement nypbotImage = driver.FindElement(By.XPath(".//*[@id='botDiv']/div/img"));
                nypbotImage.Click();

                Thread.Sleep(600); // give animation some mercy

                // wait for the following element is be present
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);


                // select general enquiries tab
                IWebElement generalEnquiriesTab = driver.FindElement(By.XPath(".//*[@id='l2']"));
                generalEnquiriesTab.Click();

                Thread.Sleep(500); // give animation some mercy

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                // select search textbox
                IWebElement messageTextbox = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[2]/div/input"));
                messageTextbox.Click();


                /* init excelsheet */
                List<string> whitelistWorkSheets = new List<string> { "SIT", "SEG", "SBM" };

                int questionRowNumber = 1;
                int answerRowNumber = 2; // expected reply
                int statusRowNumber = 7;
                int actualReplyRowNumber = 8; // to record incorrect replies

                int currentColumnNumber = 2;

                for (int whitelistIndex = 0; whitelistIndex < whitelistWorkSheets.Count; whitelistIndex++)
                {
                    try
                    {
                        botWorksheet = (Excel.Worksheet)botWorkbook.Worksheets.get_Item(whitelistWorkSheets[whitelistIndex]);
                        botRange = botWorksheet.UsedRange;

                        currentColumnNumber = 2; // reset starting column
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex); // sheet name does not exist
                        continue; // next sheet
                    }

                    int msgCount = 0;

                    // +1 after minus because the starting column is deleted from the total columns. could -1 then minus, but it may be a little more confusing
                    int totalQuestions = botWorksheet.Columns.Count - currentColumnNumber + 1;

                    for (int index = 0; index < totalQuestions; index++)
                    {
                        try
                        {
                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                            // track current message count + next question
                            msgCount = driver.FindElements(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/*")).Count + 1;

                            // Enter question from excelsheet into textbox
                            messageTextbox.SendKeys(Convert.ToString(botRange.Cells[questionRowNumber][currentColumnNumber].Value2));

                            // Send Question
                            messageTextbox.SendKeys(Keys.Enter);

                            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                            // Goes to div.wc-message-content > div > div  // msgCount + 1 = first reply from bot, other replies will be ignored (e.g. feedback / more question with options)
                            IWebElement msgContent = driver.FindElement(By.XPath("//*[@id='BotChat']/div/div/div[1]/div[1]/div/div[" + Convert.ToString(msgCount + 1) + "]/div[1]/div/div/div"));


                            // compare the answers without tab, enter and return carriage

                            string actualReply = TrimReply(msgContent.GetAttribute("innerText"));
                            string expectedReply = TrimReply(botRange.Cells[answerRowNumber][currentColumnNumber].Value2);

                            //string actualReply = Regex.Replace(Regex.Replace(msgContent.GetAttribute("innerText"), "\u00A0", " "), @"\t|\n|\r", "");
                            //string expectedReply = Regex.Replace(Regex.Replace(botRange.Cells[answerRowNumber][currentColumnNumber].Value2, "\u00A0", " "), @"\t|\n|\r", "");

                            if (expectedReply == actualReply)
                            {
                                botRange.Cells[statusRowNumber][currentColumnNumber].Value2 = "Correct";
                            }
                            else
                            {
                                botRange.Cells[statusRowNumber][currentColumnNumber].Value2 = "Incorrect";

                                botRange.Cells[actualReplyRowNumber][currentColumnNumber].Value2 = actualReply;
                            }

                            currentColumnNumber += 1;
                        }
                        catch (Exception ex)
                        {
                            Console.Write(ex);
                            break; // worksheet.UsedRange gets cells that are formatted, not just cells with value, thus causing a null exception when cells are formatted but have nothing in them
                        }
                    }

                    // Set worksheet to null due to some semantic errors
                    botWorksheet = null;
                    botRange = null;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        [TearDown]
        public void CloseBrowser()
        {
            // save to new excelsheet
            int count = 1;
            while (File.Exists(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx"))
            {
                count += 1;
            }

            botWorkbook.SaveAs(filePath + @"\TestResults\SampleQnATestResults_Chrome" + count + ".xlsx");

            botWorkbook.Close();

            botApp.Quit();

            // release all COM object (saving RAM)
            if (botRange != null) Marshal.ReleaseComObject(botRange);
            if (botWorksheet != null) Marshal.ReleaseComObject(botWorksheet);
            if (botWorkbook != null) Marshal.ReleaseComObject(botWorkbook);
            if (botApp != null) Marshal.ReleaseComObject(botApp);

            Thread.Sleep(3000);

            driver.Quit();
        }

        private string TrimReply(string reply)
        {
            char[] delimitersChars = { '\n', '\r' };

            Regex.Replace(reply, "\u00A0", " ");

            string[] replies = reply.Split(delimitersChars);

            string trimmedReplies = "";

            for (int i = 0; i < replies.Length; i++)
            {
                string trimmed = replies[i].Trim();

                trimmedReplies += trimmed;
            }
            Regex.Replace(TrimReply(trimmedReplies), @"\t|\n|\r", "");

            return trimmedReplies;
        }
    }
}
