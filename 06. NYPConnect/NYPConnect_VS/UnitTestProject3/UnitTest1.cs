﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using Excel = Microsoft.Office.Interop.Excel;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using System.Runtime.InteropServices;

namespace UnitTestProject3
{
    
    class UnitTest1
    {
        AppiumDriver<AndroidElement> driver;
        AppiumOptions appiumOptions = new AppiumOptions();

        Excel.Application nypConnectApp;
        Excel.Workbook nypConnectWorkbook;
        Excel._Worksheet nypConnectWorksheet;

        readonly string filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\..\..\..\..\..";

        List<string> whitelistWorkSheets = new List<string> { "ResultOfChangedPWD", "ResultOfBookingFacilities", "ResultOfCancelEticket"};

        readonly int studentNameRow = 1;
        readonly int datetime = 2;
        readonly int statusRow = 3;

        [SetUp]
        public void Setup()
        {
            appiumOptions.AddAdditionalCapability(MobileCapabilityType.PlatformName, "Android");
            appiumOptions.AddAdditionalCapability(MobileCapabilityType.NoReset, true); //So that you do not need to login again
            appiumOptions.AddAdditionalCapability(MobileCapabilityType.DeviceName, "Android Emulator");

            appiumOptions.AddAdditionalCapability(MobileCapabilityType.App, @"C:\Users\L33535.NYPSIT\Documents\FYPJ\06. NYPConnect\NYPConnect_VS\UnitTestProject3\NYPConnect-sit-2019.10.15.apk");
            appiumOptions.AddAdditionalCapability(AndroidMobileCapabilityType.AppPackage, "sg.edu.nyp.nypconnect");
            appiumOptions.AddAdditionalCapability(AndroidMobileCapabilityType.AppActivity, "sg.edu.nyp.nypconnect.SplashActivity");
            appiumOptions.AddAdditionalCapability(AndroidMobileCapabilityType.AppWaitActivity, "sg.edu.nyp.nypconnect.MainActivity");

        }
        [Test]
        public void NYPConnect_Tests()
        {

            var rdcUrl = "http://127.0.0.1:4723/wd/hub";
            var driver = new AndroidDriver<IWebElement>(new Uri(rdcUrl), appiumOptions);

            Thread.Sleep(5000);

            nypConnectApp = new Excel.Application();
            nypConnectWorkbook = nypConnectApp.Workbooks.Open(filePath + @"\06. NYPConnect\NYPConnect2", ReadOnly: false);

            driver.FindElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']").Click();

            var studentName = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.TextView[1]").Text;
            var locator = new ByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView("
                   + "new UiSelector().text(\"Settings\"));");

            driver.FindElement(locator);
            Thread.Sleep(500);
            driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[9]")).Click();
            Thread.Sleep(500);
            
            //change pin
            driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);

            var currentPwd = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.EditText");
            currentPwd.SendKeys("654321");
            var newPwd = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.EditText");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            newPwd.SendKeys("123456");
            var cfmPwd = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.FrameLayout/android.widget.EditText");
            cfmPwd.Clear();
            cfmPwd.SendKeys("123456");

            var changePinBtn = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button");
            changePinBtn.Click();
            Thread.Sleep(600); 

            var message = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView").Text;
            nypConnectWorksheet = (Excel.Worksheet)nypConnectWorkbook.Worksheets.get_Item(whitelistWorkSheets[0]);
            var lastrow = nypConnectWorksheet.UsedRange.Rows.Count;

            nypConnectApp.UserControl = true;
            nypConnectWorksheet.Cells[lastrow + 1, studentNameRow].Value2 = studentName;
            nypConnectWorksheet.Cells[lastrow + 1, datetime].Value2 = DateTime.Now.ToString();

            if (message == "The current PIN is not correct.")
            {

                nypConnectWorksheet.Cells[lastrow + 1, statusRow].Value2 = "Failed";
                driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button").Click();

            }
            else if (message == "Change PIN successfully.Please login again with your new PIN.")
            {
                nypConnectWorksheet.Cells[lastrow + 1, statusRow].Value2 = "Successful";
            }
            driver.FindElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.FindElementByXPath("//android.widget.ImageButton[@content-desc='Open navigation drawer']").Click();
            Thread.Sleep(500);

            var eticket = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[5]");
            eticket.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var pending = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.TextView[2]");
            pending.Click();

            nypConnectApp = new Excel.Application();
            nypConnectWorkbook = nypConnectApp.Workbooks.Open(filePath + @"\06. NYPConnect\NYPConnect2", ReadOnly: false);
            nypConnectWorksheet = (Excel.Worksheet)nypConnectWorkbook.Worksheets.get_Item(whitelistWorkSheets[2]);
            lastrow = nypConnectWorksheet.UsedRange.Rows.Count;

            nypConnectApp.UserControl = true;
            nypConnectWorksheet.Cells[lastrow + 1, studentNameRow].Value2 = studentName;
            nypConnectWorksheet.Cells[lastrow + 1, datetime].Value2 = DateTime.Now.ToString();

            try
            {
                var pendingEvent = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]");
                pendingEvent.Click();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
                var more = driver.FindElementByXPath("//android.widget.TextView[@content-desc='More']");
                more.Click();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
                driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button").Click();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
                var cancelTicket = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]");
                cancelTicket.Click();
                nypConnectWorksheet.Cells[lastrow + 1, statusRow].Value2 = "Successful";

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                nypConnectWorksheet.Cells[lastrow + 1, statusRow].Value2 = "Failed";

            }

            driver.FindElementByXPath("//android.widget.ImageButton[@content-desc='Open navigation drawer']").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            var facilities = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[4]");
            facilities.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.FrameLayout/android.view.ViewGroup/android.widget.GridView/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.FindElementByXPath("//com.prolificinteractive.materialcalendarview.f[@content-desc='Calendar']/android.widget.CheckedTextView[23]").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.GridView[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            message = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView").Text;

            driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button").Click();

            nypConnectApp = new Excel.Application();
            nypConnectWorkbook = nypConnectApp.Workbooks.Open(filePath + @"\06. NYPConnect\NYPConnect2", ReadOnly: false);
            nypConnectWorksheet = (Excel.Worksheet)nypConnectWorkbook.Worksheets.get_Item(whitelistWorkSheets[1]);
            lastrow = nypConnectWorksheet.UsedRange.Rows.Count;

            nypConnectApp.UserControl = true;
            nypConnectWorksheet.Cells[lastrow + 1, studentNameRow].Value2 = studentName;
            nypConnectWorksheet.Cells[lastrow + 1, datetime].Value2 = DateTime.Now.ToString();

            if (message.Contains("Your booking is successful.")) ;
            {
                nypConnectWorksheet.Cells[lastrow + 1, statusRow].Value2 = "Successful";

            }
            
            CloseExcel();
        }
        /*
        [Test]
        public void Facilities()
        {
            var rdcUrl = "http://127.0.0.1:4723/wd/hub";
            var driver = new AndroidDriver<IWebElement>(new Uri(rdcUrl), appiumOptions);

            driver.FindElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']").Click();
            Thread.Sleep(500);
            var studentName = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.TextView[1]").Text;

            var facilities = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[4]");
            facilities.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.FrameLayout/android.view.ViewGroup/android.widget.GridView/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ImageView").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.FindElementByXPath("//com.prolificinteractive.materialcalendarview.f[@content-desc='Calendar']/android.widget.CheckedTextView[23]").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.GridView[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            var message = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView").Text;

            driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button").Click();

            nypConnectApp = new Excel.Application();
            nypConnectWorkbook = nypConnectApp.Workbooks.Open(filePath + @"\06. NYPConnect\NYPConnect2", ReadOnly: false);
            nypConnectWorksheet = (Excel.Worksheet)nypConnectWorkbook.Worksheets.get_Item(whitelistWorkSheets[1]);
            var lastrow = nypConnectWorksheet.UsedRange.Rows.Count;

            nypConnectApp.UserControl = true;
            nypConnectWorksheet.Cells[lastrow + 1, studentNameRow].Value2 = studentName;
            nypConnectWorksheet.Cells[lastrow + 1, datetime].Value2 = DateTime.Now.ToString();

            if (message.Contains("Your booking is successful."));
            {
                nypConnectWorksheet.Cells[lastrow + 1, statusRow].Value2 = "Successful";
    
            }

            CloseExcel();
        }

        
        [Test]
        public void Eticket()
        {

            var rdcUrl = "http://127.0.0.1:4723/wd/hub";
            var driver = new AndroidDriver<IWebElement>(new Uri(rdcUrl), appiumOptions);

           

            driver.FindElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            var studentName = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.TextView[1]").Text;

          

            var eticket = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[5]");
            eticket.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var pending = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.widget.TextView[2]");
            pending.Click();

            nypConnectApp = new Excel.Application();
            nypConnectWorkbook = nypConnectApp.Workbooks.Open(filePath + @"\06. NYPConnect\NYPConnect2", ReadOnly: false);
            nypConnectWorksheet = (Excel.Worksheet)nypConnectWorkbook.Worksheets.get_Item(whitelistWorkSheets[2]);
            var lastrow = nypConnectWorksheet.UsedRange.Rows.Count;

            nypConnectApp.UserControl = true;
            nypConnectWorksheet.Cells[lastrow + 1, studentNameRow].Value2 = studentName;
            nypConnectWorksheet.Cells[lastrow + 1, datetime].Value2 = DateTime.Now.ToString();

            try
            {
                var pendingEvent = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.support.v4.view.ViewPager/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]");
                pendingEvent.Click();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
                var more = driver.FindElementByXPath("//android.widget.TextView[@content-desc='More']");
                more.Click();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
                driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.Button").Click();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
                var cancelTicket = driver.FindElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]");
                cancelTicket.Click();
                nypConnectWorksheet.Cells[lastrow + 1, statusRow].Value2 = "Successful";

            }
            catch (Exception ex)
            {
                Console.Write(ex);
                nypConnectWorksheet.Cells[lastrow + 1, statusRow].Value2 = "Failed";

            }

            CloseExcel();
        }
        */
        private bool CloseExcel()
        {
            //Disable replace file popup 
            nypConnectApp.DisplayAlerts = false;

            nypConnectWorkbook.Saved = true;
            nypConnectWorkbook.Save();
            nypConnectWorkbook.Close();
            nypConnectApp.Quit();

            // release all COM object (Close Excel properly)
            if (nypConnectWorksheet != null) Marshal.ReleaseComObject(nypConnectWorksheet);
            if (nypConnectWorkbook != null) Marshal.ReleaseComObject(nypConnectWorkbook);
            if (nypConnectApp != null) Marshal.ReleaseComObject(nypConnectApp);

            return true; 
        }
    }
}
